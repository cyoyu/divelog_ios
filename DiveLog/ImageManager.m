//
//  ImageManager.m
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "ImageManager.h"
#import "Constant.h"

@implementation ImageManager


-(id)init
{
    self = [super init];
    
    if (self) {
        
    }
    
    return self;
}


-(void)dealloc
{
}


-(void) showImagePickerActionSheet:(UIView*)view
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"相機", @"相簿", nil];
    [actionSheet showInView:view];

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex==0){
        // 相機
        //    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        //    [imagePicker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
        //    [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        //
        //    [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];


//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }else if(buttonIndex==1){
        // 相簿
//        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//        [imagePicker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//        [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//
//        [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
//
//        imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
//        imagePicker.delegate = (id) self;
//        imagePicker.allowsEditing = NO;
//
//        
//        [self presentViewController:imagePicker animated:YES completion:^{
//            
//        }];

    }
    
}

-(void) setImageWithAssetURL:(UIImageView*)imageView assetURL:(NSString*)assetURL
{

    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *rep = [myasset defaultRepresentation];
        CGImageRef iref = [rep fullResolutionImage];
        if (iref) {
            UIImage *largeimage = [UIImage imageWithCGImage:iref];
            imageView.image = largeimage;
        }
    };
    
    
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        NSLog(@"Can't get image - %@",[myerror localizedDescription]);
    };
    
    NSURL *asseturl = [NSURL URLWithString:assetURL];
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:asseturl
                   resultBlock:resultblock
                  failureBlock:failureblock];
    
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    
    UIGraphicsBeginImageContextWithOptions(image.size, YES, 1.0);
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Draw the image with the luminosity blend mode.
    // On top of a white background, this will give a black and white image.
    [image drawInRect:imageRect blendMode:kCGBlendModeLuminosity alpha:1.0];
    
    // Get the resulting image.
    UIImage *filteredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return filteredImage;
    
//    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
//    
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
//    
//    // Create bitmap content with current image size and grayscale colorspace
//    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
//    
//    // Draw image into current context, with specified rectangle
//    // using previously defined context (with grayscale colorspace)
//    CGContextDrawImage(context, imageRect, [image CGImage]);
//    
//    // Create bitmap image info from pixel data in current context
//    CGImageRef imageRef = CGBitmapContextCreateImage(context);
//    
//    // Create a new UIImage object
//    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
//    
//    // Release colorspace, context and bitmap information
//    CGColorSpaceRelease(colorSpace);
//    CGContextRelease(context);
//    CFRelease(imageRef);
//    
//    // Return the new grayscale image
//    return newImage;
}


@end
