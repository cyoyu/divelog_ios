//
//  ThemeManager.m
//  Gay
//
//  Created by tpy on 2014/5/5.
//  Copyright (c) 2014年 POPO_INNOVATION. All rights reserved.
//

#import "ThemeManager.h"



@implementation ThemeManager


+(UILabel*) newLogLabel
{
    UILabel* newLogLabel = [UILabel new];
    newLogLabel.frame = CGRectMake(10, 5, 100, 30);
    [newLogLabel setText:@"newLogLabel"];
    [newLogLabel setFont:SYSTEM_FONT_WITH_SIZE(15)];
    //[newLogLabel setBackgroundColor:AQUAMARINE_COLOR];
    return newLogLabel;
}

+(UIScrollView*) bouncedPageScrollView;
{
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    scrollView.bounces = YES;
    scrollView.scrollEnabled = YES;
    scrollView.contentSize = CGSizeMake(SCREEN_WIDTH*2, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT);
    scrollView.pagingEnabled = YES;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.showsVerticalScrollIndicator = YES;
    scrollView.delaysContentTouches = YES;
    scrollView.canCancelContentTouches = NO;
    return scrollView;
}

+(UITextField *)newTextField
{
    UITextField *newtextField = [[UITextField alloc] init];
    newtextField.frame = CGRectMake(20, 0, 280, 44);
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 30)];
    newtextField.leftView = paddingView;
    newtextField.leftViewMode = UITextFieldViewModeAlways;
    newtextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    newtextField.font=[UIFont systemFontOfSize:18];
    return newtextField;
}



+(UIColor *)mainColor
{
    return [UIColor colorWithRed:95/255.0 green:219/255.0 blue:214/255.0 alpha:1.0];
}

+(UIColor *)mainHighlightColor
{
    return MAIN_COLOR;
}

+(UIColor *)lightGrayBgColor
{
    return [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
}

+(UIColor *)backgroundColor
{
    return [UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1.0];
}


+(UIColor *)unreadCellColor
{
    return [UIColor colorWithRed:95/255.0 green:219/255.0 blue:214/255.0 alpha:0.1];
}

+(UIColor *)noticeTextColor
{
    return [UIColor colorWithRed:223/255.0 green:236/255.0 blue:241/255.0 alpha:1.0];
}





+(UIButton *)plainButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn.frame = CGRectMake(20, 0, 280, 44);
    //    UIImage *buttonBgImage = IMAGE_FROM_BUNDLE(@"btn_no_normal");
    //    UIImage *buttonBgImageClicked = IMAGE_FROM_BUNDLE(@"btn_no_click");
    //    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    //    [btn setBackgroundImage:[buttonBgImage resizableImageWithCapInsets:edgeInsets] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[buttonBgImageClicked resizableImageWithCapInsets:edgeInsets] forState:UIControlStateHighlighted];
    //    [btn setTitleColor:DARK_GRAY_TEXT_COLOR forState:UIControlStateNormal];
    //    [btn setTitleColor:DARK_GRAY_TEXT_COLOR forState:UIControlStateHighlighted];
    
    return btn;
}


+(UIButton *)blueButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn.frame = CGRectMake(20, 0, 280, 44);
    //    UIImage *buttonBgImage = IMAGE_FROM_BUNDLE(@"btn_blue_normal");
    //    UIImage *buttonBgImageClicked = IMAGE_FROM_BUNDLE(@"btn_blue_click");
    //    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    //    [btn setBackgroundImage:[buttonBgImage resizableImageWithCapInsets:edgeInsets] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[buttonBgImageClicked resizableImageWithCapInsets:edgeInsets] forState:UIControlStateHighlighted];
    //
    //    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //
    return btn;
}

+(UIButton *)greenButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    btn.frame = CGRectMake(20, 0, 280, 44);
    //    UIImage *buttonBgImage = IMAGE_FROM_BUNDLE(@"btn_green_normal");
    //    UIImage *buttonBgImageClicked = IMAGE_FROM_BUNDLE(@"btn_green_click");
    //    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    //    [btn setBackgroundImage:[buttonBgImage resizableImageWithCapInsets:edgeInsets] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[buttonBgImageClicked resizableImageWithCapInsets:edgeInsets] forState:UIControlStateHighlighted];
    //
    //    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    return btn;
}


+(UIButton*)badgeButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    btn.titleLabel.font = [UIFont systemFontOfSize:12];
    //    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    //    [btn setBackgroundImage:[IMAGE_FROM_BUNDLE(@"number_tip") resizableImageWithCapInsets:UIEdgeInsetsMake(8, 9, 8, 9)] forState:UIControlStateNormal];
    //    btn.adjustsImageWhenHighlighted = NO;
    return btn;
}

//userTableViewCell
+(UIColor *)userTableViewCellNameLabelColor
{
    return MAIN_COLOR;
}

+(UIColor *)userTableViewCellInfoLabelColor
{
    return [UIColor colorWithRed:61/255.0 green:69/255.0 blue:72/255.0 alpha:1.0];
}

+(UIColor *)userTableViewCellTimestampColor
{
    return [UIColor colorWithRed:149/255.0 green:156/255.0 blue:159/255.0 alpha:1.0];
}

+(UIColor *)userTableViewCellSignatureColor
{
    return [UIColor colorWithRed:149/255.0 green:156/255.0 blue:159/255.0 alpha:1.0];
}



#pragma Navbar
+(UIColor *)navTitleColor
{
    return [UIColor whiteColor];
}

+(UIColor *)navRightItemTextColor
{
    return [UIColor colorWithRed:17/255.0 green:159/255.0 blue:221/255.0 alpha:1.0];
}


+(UIColor *)navbarBackgroundColor
{
    return [UIColor colorWithRed:95/255.0 green:219/255.0 blue:214/255.0 alpha:1.0];
}

+(UIButton *)navBackButton
{
    UIButton *backButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 31)];
    //    [backButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_back") forState:UIControlStateNormal];
    //    [backButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_back") forState:UIControlStateHighlighted];
    return backButton;
}

+(UIButton *)searchIconButton
{
    UIButton *searchButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    //    [searchButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_search") forState:UIControlStateNormal];
    return searchButton;
}

+(UIButton *)gridIconButton
{
    UIButton *gridIconButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    //    [gridIconButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_grid") forState:UIControlStateNormal];
    return gridIconButton;
}

+(UIButton *)listIconButton
{
    UIButton *listIconButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    //    [listIconButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_list") forState:UIControlStateNormal];
    return listIconButton;
}

+(UIButton *)writePostButton
{
    UIButton *listIconButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    //    [listIconButton setImage:IMAGE_FROM_BUNDLE(@"titlebar_icon_add") forState:UIControlStateNormal];
    return listIconButton;
}


#pragma textfield
+(UITextField *)styledTextField
{
    UITextField *tf = [[UITextField alloc] init];
    //    tf.frame = CGRectMake(20, 0, 280, 44);
    //    UIImage *textFieldBgImage = IMAGE_FROM_BUNDLE(@"input_bg_normal");
    //    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(4, 4, 4, 4);
    //    [tf setBackground:[textFieldBgImage resizableImageWithCapInsets:edgeInsets]];
    //
    //    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 30)];
    //    tf.leftView = paddingView;
    //    tf.leftViewMode = UITextFieldViewModeAlways;
    //    tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    //    tf.font=[UIFont systemFontOfSize:18];
    //
    return tf;
}



#pragma nearby tab
+(UIColor *)nearbyViewBackgroundColor
{
    //    return [UIColor colorWithRed:235/255.0 green:236/255.0 blue:235/255.0 alpha:1.0];
    return [UIColor whiteColor];
}

+(UIColor *)focusViewBackgroundColor
{
    return [UIColor colorWithRed:200/255.0 green:238/255.0 blue:255/255.0 alpha:1.0];
}

#pragma UserInfo
+(UIButton *)interestButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *bgImage = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn");
    //    UIImage *bgImageDown = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn_down");
    //    [btn setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[bgImageDown resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    //
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon1") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon1") forState:UIControlStateHighlighted];
    //    [btn setTitleColor:[UIColor colorWithRed:136/255.0 green:136/255.0 blue:136/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"Interest") forState:UIControlStateNormal];
    return btn;
}

+(UIButton *)notInterestButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *bgImage = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn");
    //    UIImage *bgImageDown = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn_down");
    //    [btn setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[bgImageDown resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon2") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon2") forState:UIControlStateHighlighted];
    //    [btn setTitleColor:[UIColor colorWithRed:136/255.0 green:136/255.0 blue:136/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"NotInterest") forState:UIControlStateNormal];
    return btn;
}

+(UIButton *)chatButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *bgImage = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn");
    //    UIImage *bgImageDown = IMAGE_FROM_BUNDLE(@"segcontrol_left_btn_down");
    //    [btn setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[bgImageDown resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon3") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon3") forState:UIControlStateHighlighted];
    //    [btn setTitleColor:[UIColor colorWithRed:136/255.0 green:136/255.0 blue:136/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"Chat") forState:UIControlStateNormal];
    return btn;
}

+(UIButton *)giftButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    UIImage *bgImage = IMAGE_FROM_BUNDLE(@"segcontrol_uns");
    //    UIImage *bgImageDown = IMAGE_FROM_BUNDLE(@"segcontrol_sel");
    //    [btn setBackgroundImage:[bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateNormal];
    //    [btn setBackgroundImage:[bgImageDown resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)] forState:UIControlStateHighlighted];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon4") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"nav_light_icon4") forState:UIControlStateHighlighted];
    //    [btn setTitleColor:[UIColor colorWithRed:81/255.0 green:202/255.0 blue:34/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"Gift") forState:UIControlStateNormal];
    return btn;
}


#pragma mark - Post
+(UIImageView *)postMaintabBackgroundImageView
{
    UIImageView *view = [[UIImageView alloc] init];
    //    UIImage *bgImage = IMAGE_FROM_BUNDLE(@"maintab");
    //    view.image = [bgImage resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    //    view.userInteractionEnabled = YES;
    return view;
}


+(UIButton *)likeButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"diary_icon_like_normal") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"diary_icon_like_click") forState:UIControlStateSelected];
    //    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    //    [btn setTitleColor:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleColor:[UIColor colorWithRed:92/255.0 green:210/255.0 blue:212/255.0 alpha:1.0] forState:UIControlStateDisabled];
    //    [btn setTitleColor:[UIColor colorWithRed:92/255.0 green:210/255.0 blue:212/255.0 alpha:1.0] forState:UIControlStateSelected];
    //
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"Like") forState:UIControlStateNormal];
    return btn;
}

+(UIButton *)commentButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"diary_icon_reply_normal") forState:UIControlStateNormal];
    //    [btn setImage:IMAGE_FROM_BUNDLE(@"diary_icon_reply_click") forState:UIControlStateHighlighted];
    //    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    //    [btn setTitleColor:[UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [btn setTitleColor:[UIColor colorWithRed:100/255.0 green:100/255.0 blue:100/255.0 alpha:1.0] forState:UIControlStateHighlighted];
    //
    //    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    //    [btn setTitle:LOCALIZE(@"Comment") forState:UIControlStateNormal];
    return btn;
}


+(UIImageView *)postCellBackgroundView
{
    UIImageView *bgImageView = [[UIImageView alloc] init];
    //    [bgImageView setImage: [IMAGE_FROM_BUNDLE(@"diary_bg_gray") resizableImageWithCapInsets:UIEdgeInsetsMake(4, 4, 4, 4)]];
    return bgImageView;
}


+(UIColor *)postCellHeaderInfoLabelColor
{
    return [UIColor colorWithRed:61/255.0 green:69/255.0 blue:72/255.0 alpha:1.0];
}


+(UIColor *)metaCountColor
{
    return [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1.0];
}

#pragma mark write post
+(UIButton *)addPhotoButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[btn setImage:IMAGE_FROM_BUNDLE(@"register_camera") forState:UIControlStateNormal];
    return btn;
}


#pragma mark disclosure icon
+(UIImageView*)disclosureImageView
{
    // return [[UIImageView alloc] initWithImage:IMAGE_FROM_BUNDLE(@"icon_arr_normal")];
    return [UIImageView alloc];
}

+(void)addBottomSeperatorLineOnView:(UIView*)superView
{
    CGFloat lineHeight = 0.5;
    CGFloat y = superView.frame.size.height - lineHeight;
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, y, SCREEN_WIDTH, lineHeight)];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha = 0.7;
    [superView addSubview:line];
    [superView bringSubviewToFront:line];
}

@end