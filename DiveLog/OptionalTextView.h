//
//  CreateTextView.h
//  DiveLog
//
//  Created by York on 12/19/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "MultiIconView.h"

@interface OptionalTextView : UIView

@property (nonatomic,strong) UILabel* titleLabel;


@property (nonatomic,strong) MultiIconView* iconView;

@property (nonatomic,strong) MultiIconView* onlyIconView;


@property int viewMode;
@property BOOL isEmpty;

-(void)iconMode:(NSArray*) imgArray btnTitle:(NSArray*)titles selected:(NSSet*)sets;
-(int) getIconMode;


//-(void) setContenLabelText:(NSString*)contentStr mode:(NSString*)mode;



@end
