//
//  NewLogViewController.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "EncapsulatePickerView.h"
#import "CreateTextView.h"
#import "SingleSelectionViewController.h"
#import "MapLocationViewController.h"
#import "LogObject.h"
#import "OptionalTextView.h"


@protocol NewLogDelegate <NSObject>
-(void) didAddNewLog:(LogObject*)logObject;
@end

@interface NewLogViewController : UIViewController <UIScrollViewDelegate,IconDelegate,MultiIconViewDelegate,UIImagePickerControllerDelegate>

@property (nonatomic,strong) UIScrollView* mainScrollView;
@property (nonatomic,strong) UIButton* moreButton;
@property (nonatomic,strong) LogObject* nLog;

@property (nonatomic,strong) UIView* maskView;

@property (nonatomic,strong) NSMutableDictionary* updateDictionary;

/* Required */
@property (nonatomic,strong) UIScrollView* requireView;
@property (nonatomic,strong) CreateTextView* numTextView;
@property (nonatomic,strong) CreateTextView* locationView;  // 分區域縣市
@property (nonatomic,strong) CreateTextView* dateView;      // UIPicker
@property (nonatomic,strong) CreateTextView* timeView;      // UIPicker
@property (nonatomic,strong) CreateTextView* durationView;
@property (nonatomic,strong) CreateTextView* temperatureView;
@property (nonatomic,strong) CreateTextView* weatherView;
@property (nonatomic,strong) CreateTextView* typeView;
@property (nonatomic,strong) CreateTextView* feelView;
@property (nonatomic,strong) CreateTextView* maxDepthView;
//@property (nonatomic,strong) CreateTextView* avgDepthView;
@property (nonatomic,strong) NSMutableArray* yearsArray;
@property (nonatomic,strong) NSMutableArray* monthArray;
@property (nonatomic,strong) NSMutableArray* dateArray;
@property (nonatomic,strong) NSMutableArray* hourArray;
@property (nonatomic,strong) NSMutableArray* miniArray;
@property (nonatomic,strong) NSMutableArray* durationArray;
@property (nonatomic,strong) NSMutableArray* wateTempArray;
@property (nonatomic,strong) NSMutableArray* depthArray;
@property (nonatomic,strong) NSMutableArray* pointArray;

/* Optional */
@property (nonatomic,strong) UIScrollView* optionalView;
@property (nonatomic,strong) UILabel* optionViewLabel;

@property (nonatomic,strong) UILabel* imageLabel;
@property (nonatomic,strong) UIImageView* logImageView;

@property (nonatomic,strong) OptionalTextView* portTypeView;
@property (nonatomic,strong) NSMutableSet* selectedPort;
@property (nonatomic,strong) OptionalTextView* airTypeView;
@property (nonatomic,strong) OptionalTextView* suitTypeView;
//@property (nonatomic,strong) CreateTextView* specialView;
@property (nonatomic,strong) CreateTextView* creatureView;
@property (nonatomic,strong) NSMutableSet* selectedCreature;
@property (nonatomic,strong) CreateTextView* partnerView;
@property (nonatomic,strong) CreateTextView* noteView;
@property (nonatomic,strong) CreateTextView* imageView;
//@property (nonatomic,strong) CreateTextView* curveView;
//@property (nonatomic,strong) CreateTextView* tideView;
//@property (nonatomic,strong) CreateTextView* mapView;




@property (nonatomic,strong) NSMutableDictionary *data;
@property (nonatomic,strong) EncapsulatePickerView* pickerView;

@property id<NewLogDelegate> delegate;


@end
