//
//  MapViewController.h
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <TapkuLibrary/TapkuLibrary.h>
#import "DivePoint.h"
#import "MyAnnotation.h"

@interface MapLocationViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate,UITextViewDelegate>

@property(nonatomic,strong) MKMapView* mapView;
@property(nonatomic,strong) MyAnnotation* myAnnotation;

//@property(nonatomic,strong) CLLocationManager *location;

@property(nonatomic,strong) UIImageView* pinImageView;
@property (nonatomic, strong) NSMutableArray* divePoints;

@property bool firstUpdate;
@property (copy)void (^locationSelectionCallback)(DivePoint* divepoint);

-(void)reloadAnnotation;

@end
