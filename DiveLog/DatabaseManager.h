//
//  DatabaseManager.h
//  DiveLog
//
//  Created by York on 12/12/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabasePool.h"
#import "FMDatabaseQueue.h"
#import "Constant.h"
#import "LogObject.h"
#import "DivePoint.h"

@class LogObject;

typedef void (^CallbackWithLog)(BOOL success, NSArray* logObjects);


@interface DatabaseManager : NSObject

@property (nonatomic, strong) FMDatabase* database;
@property (nonatomic, strong) FMDatabaseQueue* FMQueue;
@property (copy) dispatch_queue_t writeQueue;

-(void) fetchLogInfoWithCompletion:(CallbackWithLog)callback;
-(int) fetchNoOfDiveThisYear;
-(void) fetchDivePoints:(void(^)(NSArray* divepoints))callback;
-(void) fetchDiveLocations:(void(^)(NSArray* locations))callback;
-(void) replaceInfoLog:(NSDictionary*)dict;
-(void) deleteLogWithLogObject:(LogObject*)log;

-(void) fetchDivePointsFromPointDB:(void(^)(NSArray* locations))callback;
-(void) replaceIntoDivePointDB:(NSDictionary*)dict;


@end
