//
//  LogInfoView.m
//  DiveLog
//
//  Created by York on 12/11/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "LogInfoView.h"

@implementation LogInfoView

#define leftMargin 5
#define topMargin 5

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.frame = frame;
    //    self.layer.borderWidth = 0.5;
    _userLevel = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin, frame.size.width-2*leftMargin, 30)];
    [_userLevel setText:@"CMAS Advanced Open Water"];
    [_userLevel setFont:SYSTEM_FONT_WITH_SIZE(12)];
    _userLevel.numberOfLines = 1;
    [_userLevel sizeToFit];
    
    _userTotalDive = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, _userLevel.frame.origin.y+_userLevel.frame.size.height, _userLevel.frame.size.width, 30)];
    [_userTotalDive setText:@"- Total Dive : 500"];
    [_userTotalDive setFont:SYSTEM_FONT_WITH_SIZE(12)];
    _userTotalDive.numberOfLines = 1;
    [_userTotalDive sizeToFit];
    
    _userTotalDiveThisYear = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, _userTotalDive.frame.origin.y+_userTotalDive.frame.size.height, _userTotalDive.frame.size.width, 30)];
    [_userTotalDiveThisYear setText:@"- Total Dive This Year : 240"];
    [_userTotalDiveThisYear setFont:SYSTEM_FONT_WITH_SIZE(12)];
    _userTotalDiveThisYear.numberOfLines = 1;
    [_userTotalDiveThisYear sizeToFit];
    
    _userAge = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, _userTotalDiveThisYear.frame.origin.y+_userTotalDiveThisYear.frame.size.height, _userTotalDiveThisYear.frame.size.width, 30)];
    [_userAge setText:@"- Age : 26"];
    [_userAge setFont:SYSTEM_FONT_WITH_SIZE(12)];
    _userAge.numberOfLines = 1;
    [_userAge sizeToFit];
    
    
    
    [_userLevel setBackgroundColor:[UIColor grayColor]];
    [_userTotalDive setBackgroundColor:[UIColor grayColor]];
    [_userTotalDiveThisYear setBackgroundColor:[UIColor grayColor]];
    [_userAge setBackgroundColor:[UIColor grayColor]];
    
    [self addSubview:_userLevel];
    [self addSubview:_userTotalDive];
    [self addSubview:_userTotalDiveThisYear];
    [self addSubview:_userAge];
    
    return self;
}

@end
