//
//  UIImage+Helper.h
//  DiveLog
//
//  Created by York on 1/24/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)


- (UIImage *) toGrayscale;

@end
