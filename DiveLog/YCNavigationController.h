//
//  YCNavigationController.h
//  DiveLog
//
//  Created by York on 4/23/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YCNavigationController : UINavigationController <UINavigationControllerDelegate>
@property (strong, nonatomic) UIPercentDrivenInteractiveTransition* interactionController;

@end
