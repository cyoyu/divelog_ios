//
//  MapViewController.h
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MyAnnotation.h"

@interface MapViewController : UIViewController

@property(nonatomic,strong) MKMapView* mapView;
@property(nonatomic,strong) UIImageView* pinImageView;
@property(nonatomic,strong) NSArray* divePoints;

@end
