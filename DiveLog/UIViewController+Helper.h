//
//  UIViewController+Helper.h
//  DiveLog
//
//  Created by York on 12/12/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Helper)

-(void) configureViewForIOS7;
-(void) addCustomNavigationBackButton;


@end
