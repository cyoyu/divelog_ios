//
//  MyAnnotation.h
//  DiveLog
//
//  Created by York on 1/25/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MyAnnotation : MKPointAnnotation

@property int tag;
@end
