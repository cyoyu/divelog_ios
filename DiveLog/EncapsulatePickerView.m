//
//  EncapsulatePickerView.m
//  DiveLog
//
//  Created by York on 12/18/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "EncapsulatePickerView.h"

@implementation EncapsulatePickerView

@synthesize pickerData;
@synthesize picker;
@synthesize pickerMode;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    pickerData = [@[@[@"Item 1", @"Item 2"],@[@"cool",@"yo"]] mutableCopy];
    
    picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    picker.delegate = self;
    picker.dataSource = self;
    picker.showsSelectionIndicator = YES;
    [picker setBackgroundColor:WHITE_COLOR];
    
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity:3];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0,0,SCREEN_WIDTH,35)];
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStyleBordered target:self action:@selector(doneCategory)];
    UIBarButtonItem *mid = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                        style:UIBarButtonItemStyleBordered target:self action:@selector(cancelCategory)];
    [buttons addObject:barButtonCancel];
    [buttons addObject:mid];
    [buttons addObject:barButtonDone];
    [toolBar setItems:buttons animated:NO];
    barButtonDone.tintColor=[UIColor blackColor];
    [picker addSubview:toolBar];
    
    [self addSubview:picker];
    [self addSubview:toolBar];
    self.alpha = 0;
    
    return self;
}

- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return pickerData.count;
}

- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[pickerData objectAtIndex:component] count];
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [pickerData objectAtIndex:component][row];
}


-(void) showPickerWithTwoColumn:(NSArray*)data oneOffset:(int)oneOffset twoOffset:(int)twoOffset withCompletion:(void (^)(BOOL done, int selectedLowIndex, int selectedHighIndex)) callback
{
    pickerData = [data mutableCopy];
    [picker reloadAllComponents];
    pickerMode  = pickerData.count;
    _twoColumnPickerCallback = callback;
    [picker selectRow:oneOffset inComponent:0 animated:YES];
    [picker selectRow:twoOffset inComponent:1 animated:YES];
    
    [self showPicker];
}

-(void) showPickerWithOneColumn:(NSArray*)data oneOffset:(int)oneOffset withCompletion:(void (^)(BOOL done, int selectedIndex)) callback
{
    pickerData = [data mutableCopy];
    [picker reloadAllComponents];
    pickerMode  = pickerData.count;
    _oneColumnPickerCallback = callback;
    [picker selectRow:oneOffset inComponent:0 animated:YES];
    
    [self showPicker];
}

-(void) showPickerWithThreeColumn:(NSArray*)data oneOffset:(int)oneOffset twoOffset:(int)twoOffset threeOffset:(int)threeOffset withCompletion:(void (^)(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex)) callback
{
    pickerData = [data mutableCopy];
    [picker reloadAllComponents];
    pickerMode  = pickerData.count;
    _threeColumnPickerCallback = callback;
    [picker selectRow:oneOffset inComponent:0 animated:YES];
    [picker selectRow:twoOffset inComponent:1 animated:YES];
    [picker selectRow:threeOffset inComponent:2 animated:YES];
    [self showPicker];
}


-(void) showPicker{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    CGAffineTransform transfrom = CGAffineTransformMakeTranslation(0, -SCREEN_HEIGHT/2);
    self.transform = transfrom;
    self.alpha = self.alpha * (-1) + 1;
    [UIView commitAnimations];
}

-(void) hidePicker {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.6];
    CGAffineTransform transfrom = CGAffineTransformMakeTranslation(0, SCREEN_HEIGHT/2);
    self.transform = transfrom;
    self.alpha = self.alpha * (-1) + 1;
    [UIView commitAnimations];
}


-(void) doneCategory{
    [self hidePicker];
    if(pickerMode==1){
        _oneColumnPickerCallback(YES,[picker selectedRowInComponent:0]);
    }else if(pickerMode==2){
        _twoColumnPickerCallback(YES,[picker selectedRowInComponent:0],[picker selectedRowInComponent:1]);
    }else{
        _threeColumnPickerCallback(YES,[picker selectedRowInComponent:0],[picker selectedRowInComponent:1],[picker selectedRowInComponent:2]);
    }
}

-(void) cancelCategory{
    [self hidePicker];
    if(pickerMode==1){
        _oneColumnPickerCallback(NO,[picker selectedRowInComponent:0]);
    }else if(pickerMode==2){
        _twoColumnPickerCallback(NO,[picker selectedRowInComponent:0],[picker selectedRowInComponent:1]);
    }else{
        _threeColumnPickerCallback(NO,[picker selectedRowInComponent:0],[picker selectedRowInComponent:1],[picker selectedRowInComponent:2]);
    }
}



@end
