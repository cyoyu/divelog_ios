
#import <Foundation/Foundation.h>
#import "BlocksKit+UIKit.h"
#import "FMDatabase.h"
#import "UIViewController+Helper.h"
#import "Singleton.h"
#import "ThemeManager.h"
#import "JSONKit.h"


#define SANDBOX_MODE 1
#ifdef SANDBOX_MODE
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

// Mode for create new log
#define ICONMODE @"ICONMODE"
#define REQUIRED @"REQUIRED"
#define OPTIONAL @"OPTIONAL"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

// Font
#define SYSTEM_FONT_WITH_SIZE(SIZE) [UIFont systemFontOfSize:SIZE]
#define BOLD_FONT_WITH_SIZE(SIZE) [UIFont boldSystemFontOfSize:SIZE]

// Singleton
#define SINGLETON [Singleton sharedInstance]
#define DATABASE [[Singleton sharedInstance] dbManager]
#define GET_DEFAULT(KEY) [[[Singleton sharedInstance] defaults] objectForKey:KEY]
#define DEFAULTS [[Singleton sharedInstance] defaults]
#define IMAGE_MANAGER [[Singleton sharedInstance] imgManager]

// Format
#define FLOAT_TO_NUMBER(x) [NSNumber numberWithFloat:x]
#define INT_TO_NUMBER(x) [NSNumber numberWithInt:x]
#define FLOAT_TO_STRING(x) [NSString stringWithFormat:@"%.1lf", x]
#define INT_TO_STRING(x) [NSString stringWithFormat:@"%d", x]
#define STRING_CASCADE(x,y) [NSString stringWithFormat:@"%@%@", x,y]
#define STRING_CASCADE_THREE(x,y,z) [NSString stringWithFormat:@"%@%@%@", x,y,z]
#define STRING_TWO_DIGIT(x) [NSString stringWithFormat:@"%02d", x]
#define STRING_TO_URL(str) [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]


// User Default
#define NAME @"NAME"
#define BIRTHDAY @"BIRTHDAY"
#define GENDER @"GENDER"
#define LINCENSE @"LINCENSE"
#define SYSTEM @"SYSTEM"
#define TOTAL_DIVE @"TOTAL_DIVE"

#define LAST_DIVE_DATE @"LAST_DIVE_DATE"
#define LAST_DIVE_TIME @"LAST_DIVE_TIME"
#define LAST_TEMP @"LAST_TEMP"
#define LAST_WEATHER @"LAST_WEATHER"
#define LAST_TYPE @"LAST_TYPE"
#define LAST_FELL @"LAST_FELL"
#define LAST_MAIN_TAB @"LAST_MAIN_TAB"
#define TOTAL_DIVE_THIS_YEAR @"TOTAL_DIVE_THIS_YEAR"

#define ADD_DIVE_NUM @"ADD_DIVE_NUM"
#define ADD_DIVE_LOCATION @"ADD_DIVE_LOCATION"
#define ADD_DIVE_POINT @"ADD_DIVE_POINT"
#define ADD_DIVE_DATE @"ADD_DIVE_DATE"
#define ADD_DIVE_TIME @"ADD_DIVE_TIME"
#define ADD_DIVE_DATETIME @"ADD_DIVE_DATETIME"
#define ADD_DIVE_DURATION @"ADD_DIVE_DURATION"
#define ADD_DIVE_TEMP @"ADD_DIVE_TEMP"
#define ADD_DIVE_MAXDEPTH @"ADD_DIVE_MAXDEPTH"
#define ADD_DIVE_AVGDEPTH @"ADD_DIVE_AVGDEPTH"
#define ADD_DIVE_WEATHER @"ADD_DIVE_WEATHER"
#define ADD_DIVE_FEEL @"ADD_DIVE_FEEL"
#define ADD_DIVE_LATITUDE @"ADD_DIVE_LATITUDE"
#define ADD_DIVE_LONGITUDE @"ADD_DIVE_LONGITUDE"
#define ADD_DIVE_TYPE @"ADD_DIVE_TYPE"
#define ADD_DIVE_COLOR @"ADD_DIVE_COLOR"

/* Color Module */
#define MAIN_COLOR [UIColor colorWithRed:68/255.0 green:72/255.0 blue:80/255.0 alpha:1.0]
#define DARK_GRAY [UIColor colorWithRed:68.0/255.0 green:72.0/255.0 blue:80.0/255.0 alpha:1.0f]
#define DARK_BLUE_GRAY [[UIColor colorWithRed:68.0/255.0 green:72.0/255.0 blue:80.0/255.0 alpha:1.0f]
#define NAVY [UIColor colorWithRed:0.0/255.0 green:79.0/255.0 blue:146.0/255.0 alpha:1.0f]
#define LIGHT_NAVY_BLUE [UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:1.0f]
#define LIGHT_RED [UIColor colorWithRed:224.0/255.0 green:28.0/255.0 blue:74.0/255.0 alpha:1.0f]
#define AQUAMARINE_LIGHT_COLOR [UIColor colorWithRed:37.0/255.0 green:207.0/255.0 blue:190.0/255.0 alpha:1.0]
#define BG_LIGHT_GRAY_COLOR [UIColor colorWithRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0]
#define PINK_COLOR [UIColor colorWithRed:255.0/255.0 green:181.0/255.0 blue:197.0/255.0 alpha:1.0]
#define ORANGE_COLOR [UIColor colorWithRed:255.0/255.0 green:107.0/255.0 blue:90.0/255.0 alpha:1.0]
#define AQUAMARINE_COLOR [UIColor colorWithRed:37.0/255.0 green:174.0/255.0 blue:190.0/255.0 alpha:1.0]
#define LIGHT_YELLOW_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:170.0/255.0 alpha:1.0]
#define GRAY_COLOR [UIColor colorWithRed:161/255.0 green:161/255.0 blue:161/255.0 alpha:1.0]
#define WHITE_COLOR [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define BLACK_COLOR [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]
#define LIGHT_GRAY_COLOR [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0]

/* WINDOW SIZE */
#define NAVI_BAR_HEIGHT 44.0
#define TAB_BAR_HEIGHT 44.0
#define STATUS_BAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height
#define STATUS_BAR_WIDTH [UIApplication sharedApplication].statusBarFrame.size.width

/* CREATOR */
#define CREATE_MUTABLE_DICTIONARY [[NSMutableDictionary alloc] init]
#define CREATE_MUTABLE_ARRAY [[NSMutableArray alloc] init]
#define CREATE_MUTABLE_STRING [[NSMutableString alloc] init]
#define CREATE_MUTABLE_DATA [[NSMutableData alloc] init]
#define CREATE_MUTABLE_SET [[NSMutableSet alloc] init]
#define CREATE_MUTABLE_ORDERED_SET [[NSMutableOrderedSet alloc] init]


/* MODE */
#define TEXT_MODE 100
#define BUTTON_MODE 200

/* OPTIOBVIEW MODE */
#define MULTI_SELECT 100
#define SINGLE_SELECT 200

/* DATA */
#define MONTHES_ARRAY @[@"1月",@"2月",@"3月",@"4月",@"5月",@"6月",@"7月",@"8月",@"9月",@"10月",@"11月",@"12月"]

#define UserArray  @[@"location",@"divePoint",@"date",@"time",@"duration",@"temperature",@"type",@"feel",@"maxDepth",@"avgDepth",@"port",@"air",@"suit",@"special",@"creature",@"image",@"curve",@"partner",@"note",@"map_latitude",@"map_longitude",@"tid"]
#define UserDic @{@"No":@"",@"location":@"",@"divePoint":@"",@"date":@"",@"time":@"",@"duration":@"",@"temperature":@"",@"type":@"",@"feel":@"",@"maxDepth":@"",@"avgDepth":@"",@"port":@"",@"air":@"",@"suit":@"",@"special":@"",@"creature":@"",@"image":@"",@"curve":@"",@"partner":@"",@"note":@"",@"map_latitude":@"",@"map_longitude":@"",@"tid":@""}


#define Emotion @[@"fuck",@"bad",@"normal",@"great",@"amazing"]
#define Weather @[@"huge_rain",@"rain",@"cloud",@"sun_cloud",@"sun"]
#define Scenery @[@"wreck",@"sandy",@"cliff",@"single_reef",@"artificial",@"cave"]
#define Type @[@"boat_dive",@"shore_dive", @"night_dive"]
#define Air @[@"air",@"nitrox32",@"nitrox36"]
#define Suits @[@"wet_3mm",@"wet_5mm",@"dry_suit"]
#define Creatures @[]

#define WHITE @"white"
#define RED @"red"
#define BLUE @"blue"
#define GREEN @"green"
#define Color @[WHITE,RED,BLUE,GREEN]


/* JSONKit */
#define TO_JSON(DICT_OR_ARRAY) [DICT_OR_ARRAY JSONStringWithOptions: JKSerializeOptionEscapeUnicode error:nil]



