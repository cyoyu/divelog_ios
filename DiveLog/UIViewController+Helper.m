//
//  UIViewController+Helper.m
//  DiveLog
//
//  Created by York on 12/12/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "UIViewController+Helper.h"

@implementation UIViewController (Helper)


-(void)configureViewForIOS7
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [self setNeedsStatusBarAppearanceUpdate];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(self.navigationController) {
        self.navigationController.navigationBar.translucent = NO;
    }
}

-(void) addCustomNavigationBackButton
{
    self.navigationItem.hidesBackButton=YES;
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(self.navigationController!=nil && self.navigationController.viewControllers.count>1) {
        // pop
        [backButton addTarget:self action:@selector(myPopHandler) forControlEvents:UIControlEventTouchUpInside];
    } else {
        // dismiss
        [backButton addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [backButton setImage:[UIImage imageNamed:@"titlebar_icon_back"] forState:UIControlStateNormal];
    // [backButton setImage:[SINGLETON loadCachedImage:@"titlebar_icon_back" withMethod:LOAD_FROM_BUNDLE] forState:UIControlStateHighlighted];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    
    backButton.frame= CGRectMake(0.0, 0.0, 40, 40);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

-(void)dismissAction
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(void) myPopHandler
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
