//
//  IconButtonView.h
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@protocol MultiIconViewDelegate <NSObject>
-(void)didClickMultiIcon:(int)btnMode onView:(UIView*)view;
@end

@interface MultiIconView : UIView

@property(nonatomic,strong) NSMutableArray* btnsArray;
@property int btnMode;
@property int viewMode;

@property id<MultiIconViewDelegate> delegate;

-(void) reloadUIwithImages:(NSArray*)imgArrays btnTitle:(NSArray*)titles selected:(NSSet*)set;

@end
