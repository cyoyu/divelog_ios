//
//  LogCell.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "LogCell.h"
#import "Constant.h"


@implementation LogCell

// 初始Cell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

    //    _numberOfDive = [ThemeManager newLogLabel];
    //    _numberOfDive.frame = CGRectMake(5, 5, 40, 20);
    
    _dateLabel = [ThemeManager newLogLabel];
    _dateLabel.frame = CGRectMake(10, 7, 70, 20);
    _dateLabel.numberOfLines = 1;
    [_dateLabel setFont:SYSTEM_FONT_WITH_SIZE(12)];
    
    _timeStampLabel = [ThemeManager newLogLabel];
    _timeStampLabel.frame = CGRectMake(100, 7, 70, 20);
    _timeStampLabel.numberOfLines = 1;
    [_timeStampLabel setFont:SYSTEM_FONT_WITH_SIZE(12)];

    _locationLabel = [ThemeManager newLogLabel];
    _locationLabel.frame = CGRectMake(165, 5, 150, 20);
    _locationLabel.numberOfLines = 1;
    
    _divePointLabel = [ThemeManager newLogLabel];
    _divePointLabel.frame = CGRectMake(200, 5, 200, 20);
    _divePointLabel.numberOfLines = 1;
    
    _typeImageView = [UIImageView new];
    _typeImageView.frame = CGRectMake(220, 25, 25, 25);
    _feelImageView = [UIImageView new];
    _feelImageView.frame = CGRectMake(250, 25, 25, 25);
    _weatherImageView = [UIImageView new];
    _weatherImageView.frame = CGRectMake(280, 25, 25, 25);
    

    _durationLabel = [ThemeManager newLogLabel];
    _durationLabel.frame = CGRectMake(155, 25, 50, 20);
    _durationLabel.numberOfLines = 1;
    
    _temperatureLabel = [ThemeManager newLogLabel];
    _temperatureLabel.frame = CGRectMake(5, 25, 50, 20);
    _temperatureLabel.numberOfLines = 1;

    _maxDepthLabel = [ThemeManager newLogLabel];
    _maxDepthLabel.frame = CGRectMake(60, 25, 50, 20);
    _maxDepthLabel.numberOfLines = 1;


    _mainImageView = [UIImageView new];
    _mainImageView.frame = CGRectMake(0, 0, 30, 30);
    [_mainImageView setBackgroundColor:MAIN_COLOR];
    
    [self setBackgroundColor:BG_LIGHT_GRAY_COLOR ];
    
    [self.contentView addSubview:_dateLabel];
    [self.contentView addSubview:_locationLabel];
    [self.contentView addSubview:_divePointLabel];
    [self.contentView addSubview:_timeStampLabel];
    [self.contentView addSubview:_durationLabel];
    [self.contentView addSubview:_temperatureLabel];
    [self.contentView addSubview:_maxDepthLabel];
    [self.contentView addSubview:_typeImageView];
    [self.contentView addSubview:_feelImageView];
    [self.contentView addSubview:_weatherImageView];
    [self.contentView addSubview:_mainImageView];

    return self;
}

// 讀取檔案
-(void) reloadUI:(LogObject*)logObject
{
    
    
    [_dateLabel setText: logObject.date ];
    [_dateLabel sizeToFit];
    _dateLabel.frame = CGRectMake(_dateLabel.frame.origin.x, _dateLabel.frame.origin.y, _dateLabel.frame.size.width, _dateLabel.frame.size.height);

    [_timeStampLabel setText:logObject.beginTime];
    [_timeStampLabel sizeToFit];
    _timeStampLabel.frame = CGRectMake(_dateLabel.frame.origin.x+ _dateLabel.frame.size.width+10, _timeStampLabel.frame.origin.y, _timeStampLabel.frame.size.width, _timeStampLabel.frame.size.height);
    
    [_divePointLabel setText: logObject.divePoint ];
    [_divePointLabel sizeToFit];
    _divePointLabel.frame = CGRectMake(SCREEN_WIDTH-_divePointLabel.frame.size.width-10, _divePointLabel.frame.origin.y, _divePointLabel.frame.size.width, _divePointLabel.frame.size.height);

    //[_numberOfDive setText:STRING_CASCADE(@"NO:" ,INT_TO_STRING(logObject.number))];
    [_locationLabel setText: logObject.location ];
    [_locationLabel sizeToFit];
    _locationLabel.frame = CGRectMake(_divePointLabel.frame.origin.x-_locationLabel.frame.size.width-10, _locationLabel.frame.origin.y, _locationLabel.frame.size.width, _locationLabel.frame.size.height);

    
//    CGRect textRect = [_locationLabel.text boundingRectWithSize:CGSizeMake(_locationLabel.frame.size.width, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : _locationLabel.font} context:nil];
//    _locationLabel.frame = CGRectMake(_locationLabel.frame.origin.x, _locationLabel.frame.origin.y, textRect.size.width, textRect.size.height);
    
    [_divePointLabel setText: logObject.divePoint ];
    [_divePointLabel sizeToFit];
    
    [_dateLabel setText: logObject.date ];
    [_dateLabel sizeToFit];

    [_timeStampLabel setText:logObject.beginTime];
    [_timeStampLabel sizeToFit];

    [_durationLabel setText:STRING_CASCADE(INT_TO_STRING(logObject.duration),@"分鐘") ];
    [_durationLabel sizeToFit];

    [_temperatureLabel setText: STRING_CASCADE(FLOAT_TO_STRING(logObject.temperature),@"度") ];
    [_temperatureLabel sizeToFit];

    
    [_maxDepthLabel setText:STRING_CASCADE(FLOAT_TO_STRING(logObject.maxDepth),@"米") ];
    [_maxDepthLabel sizeToFit];

    DLog(@"%@",FLOAT_TO_STRING(logObject.maxDepth));
    DLog(@"%@",FLOAT_TO_STRING(logObject.temperature));
    

    [_typeImageView setImage:[UIImage imageNamed:logObject.type]];
    [_feelImageView setImage:[UIImage imageNamed:logObject.feel]];
    [_weatherImageView setImage:[UIImage imageNamed:logObject.weather]];
    
    if(![logObject.picture isEqualToString:@""]){
        [IMAGE_MANAGER setImageWithAssetURL:_mainImageView assetURL:logObject.picture ];
    }
    

    
}



@end
