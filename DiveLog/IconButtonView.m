//
//  IconButtonView.m
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "IconButtonView.h"
#import "UIImage+Helper.h"

@implementation IconButtonView

@synthesize delegate;
@synthesize viewMode;


#define iconNumForLine 6
#define iconWidth 25
#define iconMargin 10

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    _btnMode = 0;
    viewMode = SINGLE_SELECT;
    _btnsArray = CREATE_MUTABLE_ARRAY;
    return self;
}

-(void) reloadUIwithImages:(NSArray*)imgArrays
{
    int nextX=iconNumForLine;
    int lines = ((int)([imgArrays count]-1)/(iconNumForLine))+1;
    int lineIndex = 0;
    int widthCount = [imgArrays count]>iconNumForLine? iconNumForLine:(int)[imgArrays count];
    
    for(int i=0;i<[imgArrays count];i++){
        
        UIButton* btn = [[UIButton alloc]initWithFrame:CGRectMake(nextX,lineIndex*(iconWidth+5)+5, iconWidth, iconWidth)];
        [btn setBackgroundImage:[[UIImage imageNamed:imgArrays[i]] toGrayscale]  forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:imgArrays[i]] forState:UIControlStateSelected];
        nextX += iconWidth+10;
        [btn setTag:i];
        btn.alpha = 0.5;
        [btn addTarget:self action:@selector(didClickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_btnsArray addObject:btn];
        [self addSubview:btn];
        if((i+1)%iconNumForLine==0)
        {
            lineIndex += 1;
            nextX = 5;
        }
    }
    self.frame = CGRectMake(SCREEN_WIDTH-widthCount*(iconWidth+10)-20,self.frame.origin.y, widthCount*(iconWidth+10), (iconWidth+5)*lines+1);
}


-(void) reloadUIwithImages:(NSArray*)imgArrays withMode:(int)mode
{
    [self reloadUIwithImages:imgArrays];

    if(viewMode==SINGLE_SELECT){
        _btnMode = mode;
        [self changeMode];
    }
}

-(void) changeMode
{
    for(int i=0;i<[_btnsArray count];i++){
        UIButton* btn = [_btnsArray objectAtIndex:i];
        if(i!=_btnMode){
            [btn setSelected:NO];
            btn.alpha = 0.5;
        }else{
            [btn setSelected:YES];
            btn.alpha = 1.0;
        }
    }
}


-(void) didClickBtn:(id)sender
{
    UIButton* clickedBtn = sender;
    _btnMode = (int)clickedBtn.tag;
    if(viewMode ==SINGLE_SELECT){
        [self changeMode];
    } else if(viewMode == MULTI_SELECT){
        if(clickedBtn.isSelected){
            [clickedBtn setSelected:NO];
            clickedBtn.alpha = 0.5;
        }else{
            [clickedBtn setSelected:YES];
            clickedBtn.alpha = 1.0;
        }
    }
    [delegate didClickIcon:_btnMode onView:self];
}

@end
