//
//  LogDetailViewController.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "DoubleLabelView.h"
#import "YCZoomTransitionAnimator.h"

@interface LogDetailViewController : UIViewController <YCZoomTransitionAnimating,UINavigationControllerDelegate>


@property (nonatomic,strong) LogObject* logObject;
@property (nonatomic,strong) UIScrollView* mainScrollView;


@property (nonatomic,strong) DoubleLabelView* locationLabel;
@property (nonatomic,strong) DoubleLabelView* divePointLabel;
@property (nonatomic,strong) DoubleLabelView* timeStampLabel;
@property (nonatomic,strong) DoubleLabelView* dateLabel;
@property (nonatomic,strong) DoubleLabelView* durationLabel;
@property (nonatomic,strong) DoubleLabelView* temperatureLabel;
@property (nonatomic,strong) DoubleLabelView* maxDepthLabel;
//@property (nonatomic,strong) UILabel* avgDepthLabel;
@property (nonatomic,strong) DoubleLabelView* typeLabel;
@property (nonatomic,strong) DoubleLabelView* feelLabel;
@property (nonatomic,strong) DoubleLabelView* weatherLabel;


// optional
@property (nonatomic, strong) UIImageView* diveImageView;
@property (nonatomic, strong) DoubleLabelView* nitroxLabel;
@property (nonatomic, strong) DoubleLabelView* suitLabel;
@property (nonatomic, strong) DoubleLabelView* portLabel;
//@property (nonatomic, strong) UILabel* specialLabel;
@property (nonatomic, strong) UILabel* creatureLabel;
@property (nonatomic, strong) UILabel* noteLabel;
@property (nonatomic, strong) UILabel* imageLabel;
@property (nonatomic, strong) UILabel* curveLabel;
@property (nonatomic, strong) UILabel* partnerLabel;


@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactivePopTransition;


@end
