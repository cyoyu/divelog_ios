

#import <TapkuLibrary/TapkuLibrary.h>
#import <UIKit/UIKit.h>


#pragma mark - CalendarMonthViewController
@interface CalendarMonthViewController : TKCalendarMonthTableViewController

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableDictionary *dataDictionary;

- (void) generateRandomDataForStartDate:(NSDate*)start endDate:(NSDate*)end;

@end
