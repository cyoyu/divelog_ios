//
//  SingleSelectionViewController.m
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "SingleSelectionViewController.h"

@implementation SingleSelectionViewController

@synthesize mainTableView;
@synthesize items;
@synthesize singleSelectionCallback;

-(id)init
{
    
    self = [super init];
    items = CREATE_MUTABLE_ARRAY;
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneAction:)];
    
    _enterText = [ThemeManager newTextField];
    _enterText.placeholder = @"請輸入";
    [_enterText setAutocorrectionType:UITextAutocorrectionTypeNo];
    _enterText.delegate=self;
    _enterText.returnKeyType = UIReturnKeyDone;
    
    mainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT) style:UITableViewStylePlain];
    [mainTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    mainTableView.tableHeaderView = _enterText;
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    
    [self.view addSubview:mainTableView];
}


#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.accessoryType = UITableViewCellStyleDefault;
    cell.textLabel.text  = items[indexPath.row];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    singleSelectionCallback(items[indexPath.row]);
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) doneAction:(id)sender
{
    if([_enterText.text isEqualToString:@""]) return;
    
    singleSelectionCallback(_enterText.text);
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [_enterText resignFirstResponder];
    [self doneAction:textField];
    return YES;
}

@end
