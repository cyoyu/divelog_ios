//
//  SingleSelectionViewController.h
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface SingleSelectionViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong) UITableView* mainTableView;
@property (nonatomic, strong) NSMutableArray* items;
@property (nonatomic, strong) UITextField* enterText;

@property (copy)void (^singleSelectionCallback)(NSString* selected);

@end
