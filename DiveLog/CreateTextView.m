//
//  CreateTextView.m
//  DiveLog
//
//  Created by York on 12/19/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "CreateTextView.h"

@implementation CreateTextView

@synthesize titleLabel;
@synthesize contentLabel;
@synthesize arrow;
@synthesize switchTool;
@synthesize viewMode;
@synthesize keyForSwitch;
@synthesize iconView;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    viewMode = TEXT_MODE;
    
    keyForSwitch = @"";
    
    titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(10, frame.size.height/2-20, frame.size.width/2-10, 40);
    [titleLabel setText:@"標題"];
    
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel = [UILabel new];
    contentLabel.frame = CGRectMake(frame.size.width/2, frame.size.height/2-20, frame.size.width/2-30, 40);
    [contentLabel setText:@"不拘"];
    [contentLabel setTextColor:NAVY];
    
    arrow = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"btn_chat_caret_recieved"]];
    arrow.frame = CGRectMake(contentLabel.frame.origin.x+contentLabel.frame.size.width, frame.size.height/2-8, 16, 16);
//    [arrow setBackgroundColor:BLACK_COLOR];
    
    switchTool = [[UISwitch alloc] initWithFrame:CGRectMake(frame.size.width-70, frame.size.height/2-15, 60, 30)];
    [switchTool addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
    
    iconView = [[IconButtonView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 5, 0, 0)];
//    [iconView reloadUIwithImages:@[@"huge_rain2",@"rain2",@"cloud2",@"sun2"]];
    
    //    [iconView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    //        [iconView changeMode];
    //
    //    }]];
    
    iconView.hidden = YES;
    [self addSubview:iconView];
    
    
    contentLabel.numberOfLines = 0;
    contentLabel.hidden = YES;
    arrow.hidden = YES;
    switchTool.hidden = YES;
    
    [self setBackgroundColor:[UIColor whiteColor]];
    [self changeMode:viewMode];
    
    [self addSubview:titleLabel];
    [self addSubview:contentLabel];
    [self addSubview:arrow];
    [self addSubview:switchTool];
    
//    [self reloadUI:contentLabel.text mode:REQUIRED];
    
    return self;
}

-(void) changeMode:(int)mode
{
    viewMode = mode;
    
    
    if(![keyForSwitch isEqualToString:@""])
    {
        [switchTool setOn:[[DEFAULTS objectForKey:keyForSwitch] intValue]==1];
    }
    
    if(viewMode==TEXT_MODE){
        contentLabel.hidden = NO;
        arrow.hidden = YES;
        switchTool.hidden = YES;
        
    }else{
        contentLabel.hidden = YES;
        arrow.hidden = YES;
        switchTool.hidden = NO;
    }
    return ;
}

-(void)reloadUI:(NSString*) content mode:(NSString*)mode
{
    
    if([content isEqualToString:@""] || [content isEqualToString:@"0"]){
        if([mode isEqualToString:REQUIRED]){
            [contentLabel setText:@"請點擊更新(必填)"];
        }else{
            [contentLabel setText:@"請點擊更新(選填)"];
        }
    }else{
        [contentLabel setText:content];
    }
    
    
    CGRect textRect = [contentLabel.text boundingRectWithSize:CGSizeMake(SCREEN_WIDTH/2, 1000)
                                                      options:NSStringDrawingUsesLineFragmentOrigin
                                                   attributes:@{ NSFontAttributeName : contentLabel.font}
                                                      context:nil];
    if(textRect.size.height>self.frame.size.height){
        self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y, SCREEN_WIDTH, textRect.size.height+10);
        contentLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    contentLabel.frame = CGRectMake(arrow.frame.origin.x-textRect.size.width , self.frame.size.height/2-textRect.size.height/2 ,textRect.size.width,textRect.size.height);
    titleLabel.frame = CGRectMake(10, self.frame.size.height/2-20, self.frame.size.width/2-10, 40);
    arrow.frame = CGRectMake(contentLabel.frame.origin.x+contentLabel.frame.size.width, self.frame.size.height/2-8, 16, 16);

    
}

-(float) heightForCell
{
    return self.frame.size.height;
}

-(void)switchAction
{
    int isOn = switchTool.isOn==YES ?  1:0;
    if(isOn==1){
        [DEFAULTS setObject:@1 forKey:keyForSwitch];
    } else{
        [DEFAULTS setObject:@0 forKey:keyForSwitch];
    }
    [DEFAULTS synchronize];
}


-(void)iconMode:(NSArray*) imgArray withMode:(int)mode
{
    iconView.hidden = NO;
    contentLabel.hidden = YES;
    arrow.hidden = YES;
    [iconView reloadUIwithImages:imgArray withMode:(int)mode];
    
    if(iconView.frame.size.height>self.frame.size.height){
        self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y, SCREEN_WIDTH, iconView.frame.size.height+10);
    }
}

-(int) getIconMode
{
    return iconView.btnMode;
}


@end
