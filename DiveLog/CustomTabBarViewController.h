
#import <UIKit/UIKit.h>

@interface CustomTabBarViewController : UIViewController


@property(nonatomic,strong) NSMutableArray *viewControllers;
@property(nonatomic,strong) NSMutableArray *tabbars;
@property (nonatomic, weak) UIViewController *selectedViewController;
@property(nonatomic,strong) UIView* containerView;


@property int currentMode;

@end
