//
//  DatabaseManager.m
//  DiveLog
//
//  Created by York on 12/12/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "DatabaseManager.h"


@implementation DatabaseManager


-(id)init
{
    self = [super init];
    
    if (self) {
        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDir = [documentPaths objectAtIndex:0];
        NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"LogDatabase.db"];
        //NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"LogDatabase.db"];
        
        _database = [FMDatabase databaseWithPath:dbPath];
        
        if(![self.database open]) {
            DLog(@"Cannot open db.sqlite");
        }
        
        _FMQueue = [FMDatabaseQueue databaseQueueWithPath:dbPath];
        _writeQueue = dispatch_queue_create("write_queue",NULL);
        
        [self createLogTable];
        [self createUserTable];
        [self createDivePointsTable];
    }
    
    return self;
}


-(void)dealloc
{
    [_database close];
}



-(void)createUserTable
{
    [self.database executeUpdate:@"CREATE TABLE IF NOT EXISTS user (id INTEGER PRIMARY KEY AUTOINCREMENT,userID TEXT UNIQUE DEFAULT NULL,isHidden INTEGER DEFAULT NULL,isVerified INTEGER DEFAULT NULL,isCelebrity INTEGER DEFAULT NULL,vipExpiration INTEGER DEFAULT NULL, nickname TEXT DEFAULT NULL, picture TEXT DEFAULT NULL, age INT DEFAULT NULL, gender TEXT DEFAULT NULL, constellation TEXT DEFAULT NULL, height INT DEFAULT NULL, weight INT DEFAULT NULL, intro TEXT DEFAULT NULL, interest TEXT DEFAULT NULL, job TEXT DEFAULT NULL, city TEXT DEFAULT NULL, latitude FLOAT DEFAULT NULL, longitude FLOAT DEFAULT NULL, visitCount INT DEFAULT 0, followerCount INT DEFAULT 0, bid INT DEFAULT NULL,lastLogin INT DEFAULT NULL);"];
    
    // new column
    //    [_database executeUpdate:@"ALTER TABLE user ADD COLUMN followerCount INT DEFAULT 0"];
    //    [_database executeUpdate:@"CREATE UNIQUE INDEX indexUserID ON user(userID);"];
}

-(void)createLogTable
{
    
    [self.database executeUpdate:@"CREATE TABLE IF NOT EXISTS diveLog (id INTEGER PRIMARY KEY AUTOINCREMENT,color TEXT DEFAULT NULL,picture TEXT DEFAULT NULL,location TEXT DEFAULT NULL,divePoint TEXT DEFAULT NULL,date TEXT DEFAULT NULL,time TEXT DEFAULT NULL,datetime DATETIME2 DEFAULT NULL, duration FLOAT DEFAULT NULL, temperature FLOAT DEFAULT NULL, weather TEXT DEFAULT NULL, type TEXT DEFAULT NULL,  feel TEXT DEFAULT NULL, maxDepth FLOAT DEFAULT NULL, avgDepth FLOAT DEFAULT NULL, port TEXT DEFAULT NULL, air TEXT DEFAULT NULL, suit TEXT DEFAULT NULL, special TEXT DEFAULT NULL, creature TEXT DEFAULT NULL, curve TEXT DEFAULT NULL, partner TEXT DEFAULT NULL, note TEXT DEFAULT NULL, map_latitude FLOAT DEFAULT 0, map_longitude FLOAT DEFAULT 0, tid TEXT DEFAULT NULL);"];
    
    // new column
    //    [_database executeUpdate:@"ALTER TABLE diveLog ADD COLUMN followerCount INT DEFAULT 0"];
    //    [_database executeUpdate:@"CREATE UNIQUE INDEX indexUserID ON user(userID);"]; // 加入索引 幫助search
}
-(void) replaceInfoLog:(NSDictionary*)dict
{
    //    NSDictionary* dic = @{@"No":@"",@"location":@"",@"divePoint":@"",@"date":@"",@"time":@""
    //                          ,@"duration":@"",@"temperature":@"",@"type":@"",@"feel":@"",@"maxDepth":@""
    //                          ,@"avgDepth":@"",@"port":@"",@"air":@"",@"suit":@"",@"special":@""
    //                          ,@"creature":@"",@"image":@"",@"curve":@"",@"partner":@"",@"note":@""
    //                          ,@"map_latitude":@"",@"map_longitude":@"",@"tid":@""};
    NSLog(@"%@",dict);
    
    dispatch_async(self.writeQueue, ^{
        [self.FMQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            
            [db executeUpdate:@"REPLACE INTO diveLog ('id','color','picture','location', 'divePoint', 'date', 'time','datetime', 'duration', 'temperature', 'type', 'feel','weather','maxDepth','avgDepth', 'port', 'air', 'suit', 'special', 'creature', 'curve', 'partner', 'note', 'map_latitude', 'map_longitude', 'tid') VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
             dict[@"id"],
             dict[@"color"],
             dict[@"picture"],
             dict[@"location"],
             dict[@"divePoint"],
             dict[@"date"],
             dict[@"beginTime"],
             dict[@"datetime"],
             dict[@"duration"],
             dict[@"temperature"],
             dict[@"type"],
             dict[@"feel"],
             dict[@"weather"],
             dict[@"maxDepth"],
             dict[@"avgDepth"],
             dict[@"port"],
             dict[@"air"],
             dict[@"suit"],
             dict[@"special"],
             dict[@"creature"],
             dict[@"curve"],
             dict[@"partner"],
             dict[@"note"],
             dict[@"map_latitude"],
             dict[@"map_longitude"],
             dict[@"tid"] ];
            
        }];
    });
    
    
}


#pragma mark delete
-(void) deleteLogWithLogObject:(LogObject*)log
{
    dispatch_async(self.writeQueue, ^{
        [self.FMQueue inDatabase:^(FMDatabase *db) {
            [db executeUpdate:@"DELETE FROM diveLog WHERE id=?",INT_TO_STRING(log.number)];
        }];
    });
}

#pragma mark fetch

-(void) fetchLogInfoWithCompletion:(CallbackWithLog)callback
{
    NSMutableArray *logs = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *set = [db executeQuery:@"SELECT * FROM divelog ORDER BY datetime ASC"];
        while ([set next]) {
            LogObject* log = [LogObject getLogObjectWithDict:[set resultDictionary]];
            [logs addObject:log];
        }
    }];
    callback(YES,logs);
}

-(void) fetchDivePoints:(void(^)(NSArray* divepoints))callback
{
    
    NSMutableArray *logs = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *set = [db executeQuery:@"SELECT DISTINCT divePoint FROM divelog "];
        while ([set next]) {
            NSString* str = [[set resultDictionary] objectForKey:@"divePoint"];
            [logs addObject:str];
        }
    }];
    callback([logs copy]);
}


-(void) fetchDiveLocations:(void(^)(NSArray* locations))callback;
{
    NSMutableArray *logs = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *set = [db executeQuery:@"SELECT DISTINCT location FROM divelog "];
        while ([set next]) {
            NSString* str = [[set resultDictionary] objectForKey:@"location"];
            [logs addObject:str];
        }
    }];
    callback([logs copy]);
}

-(int) fetchNoOfDiveThisYear
{
    NSMutableArray *logs = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *set = [db executeQuery:@"SELECT * FROM divelog"];
        while ([set next]) {
            LogObject* log = [LogObject getLogObjectWithDict:[set resultDictionary]];
            [logs addObject:log];
        }
    }];
    return [logs count];
}



#pragma mark DivePointDB

-(void)createDivePointsTable
{
    [self.database executeUpdate:@"CREATE TABLE IF NOT EXISTS divePoints (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT DEFAULT NULL,location TEXT DEFAULT NULL,latitude FLOAT DEFAULT NULL, longitude FLOAT DEFAULT NULL,type TEXT DEFAULT NULL,info TEXT DEFAULT NULL,pictures TEXT DEFAULT NULL,creatures TEXT DEFAULT NULL,UNIQUE(latitude, longitude));"];
//    // new column
//    [_database executeUpdate:@"ALTER TABLE user ADD COLUMN followerCount INT DEFAULT 0"];
//    [_database executeUpdate:@"CREATE UNIQUE INDEX indexUserID ON user(userID);"];
}

-(void) fetchDivePointsFromPointDB:(void(^)(NSArray* locations))callback
{
    NSMutableArray *points = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *set = [db executeQuery:@"SELECT * FROM divePoints"];
        while ([set next]) {
            
            NSLog(@"%@",[set resultDictionary]);
            
            DivePoint* divepoint = [DivePoint getDivePointWithDict:[set resultDictionary]];
            [points addObject:divepoint];
        }
    }];
    callback([points copy]);
}


-(void) fetchDivePointByName:(NSString*)pointname withCompletion:(void(^)(NSArray* locations))callback
{
    NSMutableArray *points = [NSMutableArray array];
    
    [self.FMQueue inDatabase:^(FMDatabase *db) {

        FMResultSet *set = [db executeQuery:@"SELECT * FROM divePoints WHERE name=?",pointname];
        while ([set next]) {
            
            NSLog(@"%@",[set resultDictionary]);
            DivePoint* divepoint = [DivePoint getDivePointWithDict:[set resultDictionary]];
            [points addObject:divepoint];
        }
    }];
    callback([points copy]);
}

-(void) replaceIntoDivePointDB:(NSDictionary*)dict
{
    NSLog(@"%@",dict);

    dispatch_async(self.writeQueue, ^{
        [self.FMQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            
            [db executeUpdate:@"REPLACE INTO divePoints ('id', 'name','location', 'latitude', 'longitude','type','info','pictures','creatures') VALUES (?,?,?,?,?,?,?,?,?)",
             dict[@"id"],
             dict[@"name"],
             dict[@"location"],
             dict[@"latitude"],
             dict[@"longitude"],
             dict[@"type"],
             dict[@"info"],
             dict[@"pictures"],
             dict[@"creatures"]
             ];
        }];
    });

}




@end
