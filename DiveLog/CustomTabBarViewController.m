//
//  CustomTabBarController.m
//  DiveLog
//
//  Created by York on 2015/4/22.
//  Copyright (c) 2015年 yokusama. All rights reserved.
//

#import "CustomTabBarViewController.h"
#import "Constant.h"

@interface CustomTabBarViewController ()

@end

@implementation CustomTabBarViewController

#define tabbarHeight  50

@synthesize viewControllers;
- (id)init
{
    self = [super init];
    if (self != nil)
    {
        viewControllers = CREATE_MUTABLE_ARRAY;
        _tabbars = CREATE_MUTABLE_ARRAY;
        _currentMode = 0;
    
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];

    int barWidth = SCREEN_WIDTH/[viewControllers count];
    int barY = SCREEN_HEIGHT-tabbarHeight;

    _containerView = [UIView new];
    [_containerView setBackgroundColor:[UIColor whiteColor]];
    [_containerView setAutoresizingMask:(UIViewAutoresizingFlexibleWidth|
                                       UIViewAutoresizingFlexibleHeight)];
    _containerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:_containerView];
    
    for(int i=0 ; i <[viewControllers count];i++){
        UIImageView* tabbar = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nav_icon4_noraml"]];
        tabbar.userInteractionEnabled = YES;
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(didClickAction:)];
        [tabbar addGestureRecognizer:tapGesture];
        tabbar.tag = i;
        tabbar.frame = CGRectMake(i*barWidth, barY, barWidth, tabbarHeight);
        [self.view addSubview:tabbar];
    }
    
    [self reload];
}

-(void)reload
{
//    for(int i=0 ; i <[viewControllers count];i++){
//        if(_currentMode==i){
//            UIViewController* viewController = [viewControllers objectAtIndex:i];
//            [self addChildViewController:viewController];
//            [viewController didMoveToParentViewController:self];
//            [viewController didMoveToParentViewController:self];
////            [self transitionFromViewController:sourceController toViewController:destinationController duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
////            
////            viewController.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
//  //          _selectedViewController = viewController;
//        }
//    }

    
}

-(void) didClickAction:(UITapGestureRecognizer*)tapGesture
{
    
    UIImageView* tabbarView = (UIImageView*)tapGesture.view;
//    int index = _currentMode;
    _currentMode = (int)tabbarView.tag;
    DLog(@"%d",_currentMode);
//    [self reload];

    if ([self selectedViewController]) {
        [[self selectedViewController] willMoveToParentViewController:nil];
        [[[self selectedViewController] view] removeFromSuperview];
        [[self selectedViewController] removeFromParentViewController];
    }

    _selectedViewController = [viewControllers objectAtIndex:_currentMode];
    [self addChildViewController:[self selectedViewController]];
    [[[self selectedViewController] view] setFrame:[_containerView bounds]];
    [_containerView addSubview:[[self selectedViewController] view]];
    [[self selectedViewController] didMoveToParentViewController:self];
    
    
//    UIViewController *sourceController = [viewControllers objectAtIndex:index];
//    UIViewController *destinationController = [viewControllers objectAtIndex:_currentMode];
//    [self addChildViewController:destinationController];
//    [destinationController didMoveToParentViewController:self];
//    [self transitionFromViewController:sourceController toViewController:destinationController duration:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{} completion:^(BOOL finished) {
//        
//        [sourceController willMoveToParentViewController:nil];
//        [sourceController removeFromParentViewController];
//        
////        [self setUpContsraintsForDestinationCOntrollerView:destinationController.view];
////        selectedIndex = index;
////        if([self.delegate respondsToSelector:@selector(didSelectTabAtIndex:)]){
////            
////            [self.delegate didSelectTabAtIndex:selectedIndex];
//        
//            //}
//    }];
    
}

@end
