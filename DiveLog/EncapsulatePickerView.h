//
//  EncapsulatePickerView.h
//  DiveLog
//
//  Created by York on 12/18/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface EncapsulatePickerView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (nonatomic,strong) UIPickerView* picker;
@property (nonatomic,strong) NSMutableArray* pickerData;
@property int pickerMode;
@property (copy)void (^oneColumnPickerCallback)(BOOL done, int selectedIndex);
@property (copy)void (^twoColumnPickerCallback)(BOOL done, int selectedOneIndex, int selectedTwoIndex);
@property (copy)void (^threeColumnPickerCallback)(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex);


-(void) showPickerWithOneColumn:(NSArray*)data oneOffset:(int)oneOffset withCompletion:(void (^)(BOOL done, int selectedIndex)) callback;
-(void) showPickerWithTwoColumn:(NSArray*)data oneOffset:(int)oneOffset twoOffset:(int)twoOffset withCompletion:(void (^)(BOOL done, int selectedOneIndex, int selectedTwoIndex)) callback
;
-(void) showPickerWithThreeColumn:(NSArray*)data oneOffset:(int)oneOffset twoOffset:(int)twoOffset threeOffset:(int)threeOffset withCompletion:(void (^)(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex)) callback;
;


-(void) hidePicker;


@end
