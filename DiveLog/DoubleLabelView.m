//
//  DoubleLabelView.m
//  DiveLog
//
//  Created by York on 1/25/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "DoubleLabelView.h"

@implementation DoubleLabelView

@synthesize titleLabel;
@synthesize contentLabel;



-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    titleLabel =  [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 80, 40)];
    contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, 5, 120, 40)];
    [self addSubview:titleLabel];
    [self addSubview:contentLabel];
    return self;
}

@end
