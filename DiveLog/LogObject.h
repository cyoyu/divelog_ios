//
//  LogObject.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface LogObject : NSObject

// require
@property int number;
@property (nonatomic, strong) NSString* color;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* divePoint;
@property (nonatomic, strong) NSString* date;
@property (nonatomic, strong) NSString* datetime;
@property (nonatomic, strong) NSString* beginTime;
@property int duration;
@property float temperature;
@property (nonatomic, strong) NSString* weather;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* feel;
@property float maxDepth;
@property float avgDepth;

@property float latitude;
@property float longitude;



// optional
@property (nonatomic, strong) NSString* picture;
@property (nonatomic, strong) NSString* port;
@property (nonatomic, strong) NSString* air;
@property (nonatomic, strong) NSString* suit;
@property (nonatomic, strong) NSString* special;
@property (nonatomic, strong) NSString* creature;
@property (nonatomic, strong) NSString* note;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* curve;
@property (nonatomic, strong) NSArray* partner;

//@property  BOOL nitrox;

-(NSDictionary*) getDicWithLogObject;
+(LogObject*)getLogObjectWithDict:(NSDictionary*)dict;
-(void) checkCompletion:(void(^)(BOOL success,NSString* message))callback;

@end
