//
//  LogInfoView.h
//  DiveLog
//
//  Created by York on 12/11/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface LogInfoView : UIView

@property (nonatomic,strong) UILabel* userLevel;
@property (nonatomic,strong) UILabel* userSystem;
@property (nonatomic,strong) UILabel* userTotalDive;
@property (nonatomic,strong) UILabel* userTotalDiveThisYear;
@property (nonatomic,strong) UILabel* userAge;

@end
