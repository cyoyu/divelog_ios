//
//  LogObject.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "LogObject.h"

@implementation LogObject

@synthesize number;
@synthesize location;
@synthesize divePoint;
@synthesize date;
@synthesize datetime;
@synthesize beginTime;
@synthesize duration;
@synthesize temperature;
@synthesize weather;
@synthesize type;
@synthesize feel;
@synthesize maxDepth;
@synthesize avgDepth;
@synthesize latitude;
@synthesize longitude;

@synthesize port;
@synthesize air;
@synthesize suit;
@synthesize picture;
@synthesize creature;
@synthesize color;

-(id)init
{
    self = [super init];
    
    number = 0;
    location = @"";
    divePoint = @"";
    date = @"";
    datetime = @"";
    beginTime = @"";
    duration = 0;
    temperature = 0.0f;
    weather = @"";
    type = @"";
    feel = @"";
    maxDepth = 0.0f;
    avgDepth = 0.0f;
    color = WHITE;

    picture = @"";
    air = @"";
    suit = @"";
    port = @"[]";
    creature = @"[]";

    
    return self;
}


+(LogObject*)getLogObjectWithDict:(NSDictionary*)dict;
{
    LogObject* log = [LogObject new];
    
    
    
    log.number = [dict[@"id"] intValue];
    log.location = dict[@"location"];
    log.divePoint = dict[@"divePoint"];
    log.duration = [dict[@"duration"] intValue];
    log.beginTime = dict[@"time"];
    log.date = dict[@"date"];
    log.temperature = [dict[@"temperature"] floatValue] ;
    log.maxDepth = [dict[@"maxDepth"] floatValue];
    log.avgDepth = [dict[@"avgDepth"] floatValue];
    log.longitude = [dict[@"longitude"] floatValue];
    log.latitude = [dict[@"latitude"] floatValue];

    log.weather = dict[@"weather"];
    log.type = dict[@"type"];
    log.feel = dict[@"feel"];
    log.color = dict[@"color"];


    if(dict[@"picture"]!=nil && [dict[@"picture"] isKindOfClass:[NSNull class]]==NO){
        log.picture = dict[@"picture"];
    }
    if([dict[@"air"] isKindOfClass:[NSNull class]]==NO){
        log.air = dict[@"air"];
    }
    if([dict[@"suit"] isKindOfClass:[NSNull class]]==NO){
        log.suit = dict[@"suit"];
    }
    if([dict[@"port"] isKindOfClass:[NSNull class]]==NO){
        log.port = dict[@"port"];
    }
    if([dict[@"creature"] isKindOfClass:[NSNull class]]==NO){
        log.creature = dict[@"creature"];
    }
        return log;
}

-(NSDictionary*) getDicWithLogObject
{
    NSDictionary* dict = @{@"color":self.color,@"air":self.air,@"suit":self.suit,@"port":self.port,@"creature":self.creature,@"picture":self.picture,@"location":self.location,@"divePoint":self.divePoint,@"date":self.date,@"beginTime":self.beginTime,@"datetime":self.datetime,@"duration":[NSNumber numberWithInt:self.duration],@"weather":self.weather,@"type":self.type,@"feel":self.feel,@"number":[NSNumber numberWithInt:self.number],@"duration":[NSNumber numberWithInt:self.duration],@"temperature":[NSNumber numberWithFloat:self.temperature],@"maxDepth":[NSNumber numberWithFloat:self.maxDepth],@"avgDepth":[NSNumber numberWithFloat:self.avgDepth],@"latitude":[NSNumber numberWithFloat:self.latitude],@"longitude":[NSNumber numberWithFloat:self.longitude]};
    
    return dict;
}

-(void)checkCompletion:(void(^)(BOOL success ,NSString* message))callback
{
    NSArray* strChecked = @[@"location",@"divePoint",@"date",@"beginTime",@"type",@"feel",@"weather"];
    NSArray* valueChecked = @[@"number",@"duration",@"temperature",@"maxDepth"];
    NSDictionary* dict = [self getDicWithLogObject];
    for(NSString* str in strChecked){
        if ([[dict objectForKey:str] isEqualToString:@""]) {
            callback(NO,STRING_CASCADE(@"NO ",str ));
            return;
        }
    }
    for(NSString* str in valueChecked){
        if ((int)[dict objectForKey:str]==0 ) {
            
            callback(NO,STRING_CASCADE(@"NO ",str ));
            return;
        }
    }
    
    callback(YES,@"GREAT");
}


@end
