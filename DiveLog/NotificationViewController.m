//
//  NotificationViewController.m
//  DiveLog
//
//  Created by York on 2015/4/25.
//  Copyright (c) 2015年 yokusama. All rights reserved.
//

#import "NotificationViewController.h"
#import "Constant.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:0.1f]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
