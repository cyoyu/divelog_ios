//
//  IconButtonView.h
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@protocol IconDelegate <NSObject>
-(void)didClickIcon:(int)btnMode onView:(UIView*)view;
@end

@interface IconButtonView : UIView

@property(nonatomic,strong) NSMutableArray* btnsArray;
@property int btnMode;
@property int viewMode;
@property id<IconDelegate> delegate;

-(void) reloadUIwithImages:(NSArray*)imgArrays withMode:(int)mode;
-(void) reloadUIwithImages:(NSArray*)imgArrays; // For MultiSelectionView

-(void) changeMode;

@end
