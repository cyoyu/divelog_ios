//
//  Singleton.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constant.h"
#import "DatabaseManager.h"
#import "ImageManager.h"
#import "CustomTabBarViewController.h"

@class DatabaseManager;
@class ImageManager;

@interface Singleton : NSObject

@property (nonatomic, strong) DatabaseManager* dbManager;
@property (nonatomic, strong) ImageManager* imgManager;
@property (nonatomic, retain) UITabBarController* mainTabBarViewController;
@property (nonatomic, strong) NSUserDefaults* defaults;

+ (id)sharedInstance;
-(void) customizeInterface;
-(void)setup;

-(NSString*)getCurrentYear;
-(NSString*)getCurrentDate;
-(NSString*)getCurrentTime;
-(NSString*)getCurrentHour;
-(NSString*)getCurrentMinute;
-(NSString*)getCurrentMonth;
-(NSString*)getCurrentFullDate;


@property (nonatomic, strong) UIView* dialogBgView;

-(void) showTextDialog:(NSString*)title withSubtitle:(NSString*)subtitle  withDelegate:(id<UITextViewDelegate>)delegate withCompletion: (void (^)(NSString* message)) callback;
-(void) dialogAnimationUp:(int)range;

@end
