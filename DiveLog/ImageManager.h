//
//  ImageManager.h
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"

typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);



@interface ImageManager : NSObject


-(void) showImagePickerActionSheet:(UIView*)view;
-(void) setImageWithAssetURL:(UIImageView*)imageView assetURL:(NSString*)assetURL;
- (UIImage *)convertImageToGrayScale:(UIImage *)image;


@end
