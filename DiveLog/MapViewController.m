//
//  MapViewController.m
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "MapViewController.h"
#import "Constant.h"
#import <MapKit/MapKit.h>

@implementation MapViewController
- (id)init
{
    self = [super init];
    if (self != nil){
    }
    return self;
}


-(void)viewDidLoad
{
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addLocation)];

    
    _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mapView.showsUserLocation = YES;
    
//    _pinImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pink_pin"]];
//    _pinImageView.frame = CGRectMake(SCREEN_WIDTH/2-9, SCREEN_HEIGHT/2-NAVI_BAR_HEIGHT-28, 18, 28);
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:0.1f]];

    [self.view addSubview:_mapView];
    [self.view addSubview:_pinImageView];
    
    [DATABASE fetchDivePointsFromPointDB:^(NSArray *locations) {
        _divePoints = locations ;
        [self reloadAnnotation];
    }];
    
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //_pinImageView.frame = CGRectMake(SCREEN_WIDTH/2-9, SCREEN_HEIGHT/2-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-28, 18, 28);
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)reloadAnnotation
{
    int index = 0;
    MKCoordinateRegion region;
    MKCoordinateSpan mapSpan;
    mapSpan.latitudeDelta =0.055;
    mapSpan.longitudeDelta =0.055;
    region.span = mapSpan;

    for(DivePoint* points in _divePoints){
        MyAnnotation *annotation = [[MyAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake(points.latitude, points.longitude);
        annotation.title = points.name;
        annotation.subtitle = points.location;
        annotation.tag = index;
        [_mapView addAnnotation:annotation];
        index += 1;
        if(points.latitude == [GET_DEFAULT(ADD_DIVE_LATITUDE) floatValue] && points.longitude == [GET_DEFAULT(ADD_DIVE_LONGITUDE) floatValue]){
            CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(points.latitude,points.longitude);
            region.center = coordinate;
            _mapView.region = region;
            [_mapView selectAnnotation:annotation animated:FALSE];
        }
    }
}


-(void) addLocation{

    double X = _mapView.centerCoordinate.latitude;
    double Y = _mapView.centerCoordinate.longitude;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(X, Y);
    annotation.title = @"title";
    annotation.subtitle = @"subtitle";
    
    
    
    //    annotation.center = self.mapView.center;
    //    annotation.pinColor = MKPinAnnotationColorGreen;
    //
    [_mapView addAnnotation:annotation];

}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation{
    
    MKAnnotationView *view = nil;
    //MKPinAnnotationView *view=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"HotSpotsLoc"];
    
    if(annotation !=_mapView.userLocation){
        view = (MKAnnotationView *)
        [_mapView dequeueReusableAnnotationViewWithIdentifier:@"identifier"];
        if(nil == view) {
            view = [[MKAnnotationView alloc]
                     initWithAnnotation:annotation reuseIdentifier:@"identifier"]
                    ;
        }
        
        view.image = [UIImage imageNamed:@"nav_icon_active"];
        
        UIButton *btnViewVenue = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        view.rightCalloutAccessoryView=btnViewVenue;
        view.enabled = YES;
        view.canShowCallout = YES;
        view.multipleTouchEnabled = NO;
        //view.animatesDrop = YES;
        
    }       
    return view;
}





@end
