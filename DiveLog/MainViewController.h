//
//  MainViewController.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "LogInfoView.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "AssetsLibrary/AssetsLibrary.h"
#import "ALAssetsLibrary+CustomPhotoAlbum.h"

typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

@interface MainViewController : UIViewController <UIImagePickerControllerDelegate>

@property (nonatomic,strong) UIView* headerView;
@property (nonatomic,strong) UIImageView* userImageView;
@property (nonatomic,strong) UILabel* userName;
@property (nonatomic,strong) LogInfoView* logView;

@property (nonatomic,strong) UIView* footerView;

@property (nonatomic,strong) ALAssetsLibrary* library;


@end

