//
//  MKPointAnnotation+Helper.h
//  DiveLog
//
//  Created by York on 1/25/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKPointAnnotation (Helper)

@property (nonatomic, assign) NSInteger tag;

@end
