//
//  DivePoint.h
//  DiveLog
//
//  Created by POPO on 1/22/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DivePoint : NSObject

@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property float latitude;
@property float longitude;

@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* info;
@property (nonatomic, strong) NSString* pictures;  // jsonArray
@property (nonatomic, strong) NSString* creatures; // jsonArray
@property int times;
@property int lastDiveTime;

+(DivePoint*)getDivePointWithDict:(NSDictionary*)dict;
-(NSDictionary*) getDictFromDivePoint;

@end
