//
//  CreateTextView.h
//  DiveLog
//
//  Created by York on 12/19/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "IconButtonView.h"

@interface CreateTextView : UIView

@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UILabel* contentLabel;

@property (nonatomic,strong) UIImageView* arrow;
@property (nonatomic,strong) UISwitch* switchTool;
@property (nonatomic,strong) NSString* keyForSwitch;

@property (nonatomic,strong) IconButtonView* iconView;

@property int viewMode;
@property BOOL isEmpty;

-(void) changeMode:(int)mode;
-(void)reloadUI:(NSString*) content mode:(NSString*)mode;
-(float) heightForCell;
-(void)iconMode:(NSArray*) imgArray withMode:(int)mode;
-(int) getIconMode;

//-(void) setContenLabelText:(NSString*)contentStr mode:(NSString*)mode;



@end
