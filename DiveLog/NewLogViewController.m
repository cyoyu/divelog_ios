//
//  NewLogViewController.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "NewLogViewController.h"
#import "ThemeManager.h"
#import "BlocksKit+UIKit.h"


@implementation NewLogViewController

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        _nLog = [LogObject new];
        _selectedPort = [NSMutableSet new];
        _selectedCreature = [NSMutableSet new];

        _nLog.location = GET_DEFAULT(ADD_DIVE_LOCATION);
        _nLog.divePoint = GET_DEFAULT(ADD_DIVE_POINT);
        _nLog.latitude = [GET_DEFAULT(ADD_DIVE_LATITUDE) floatValue];
        _nLog.latitude = [GET_DEFAULT(ADD_DIVE_LONGITUDE) floatValue];
        _nLog.date = GET_DEFAULT(ADD_DIVE_DATE);
        _nLog.beginTime = GET_DEFAULT(ADD_DIVE_TIME);
        _nLog.datetime = GET_DEFAULT(ADD_DIVE_DATETIME);
        _nLog.duration = [GET_DEFAULT(ADD_DIVE_DURATION) intValue];
        _nLog.temperature = [GET_DEFAULT(ADD_DIVE_TEMP) intValue];
        _nLog.maxDepth = [GET_DEFAULT(ADD_DIVE_MAXDEPTH) intValue];
        _nLog.avgDepth = [GET_DEFAULT(ADD_DIVE_AVGDEPTH) intValue];
        _nLog.weather = GET_DEFAULT(ADD_DIVE_WEATHER);
        _nLog.feel = GET_DEFAULT(ADD_DIVE_FEEL);
        _nLog.type = GET_DEFAULT(ADD_DIVE_TYPE);
        _nLog.color = GET_DEFAULT(ADD_DIVE_COLOR);

        
        _yearsArray = CREATE_MUTABLE_ARRAY;
        _hourArray = CREATE_MUTABLE_ARRAY;
        _miniArray = CREATE_MUTABLE_ARRAY;
        _dateArray = CREATE_MUTABLE_ARRAY;
        _durationArray = CREATE_MUTABLE_ARRAY;
        _wateTempArray = CREATE_MUTABLE_ARRAY;
        _depthArray = CREATE_MUTABLE_ARRAY;
        _pointArray = CREATE_MUTABLE_ARRAY;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    [self dataSetting];
    
    _maskView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [_maskView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidePicker:)]];
    _maskView.hidden = YES;
    
    UIButton* doneButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 44)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    
    _mainScrollView = [ThemeManager bouncedPageScrollView];
    _mainScrollView.delegate = self;
    
    
    _pickerView = [[EncapsulatePickerView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT/2)];
    
    
    UIButton* upButton = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 30)];
    [upButton addTarget:self action:@selector(showPicker) forControlEvents:UIControlEventTouchUpInside];
    [upButton setTitle:@"UP" forState:UIControlStateNormal];
    [upButton setBackgroundColor:WHITE_COLOR];
    
    _requireView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    [_requireView setBackgroundColor:BG_LIGHT_GRAY_COLOR];
    
    
    [self reloadUIrequredView];
    [self reloadUIoptionalView];
    
    [_mainScrollView addSubview:_requireView];
    [_mainScrollView addSubview:_optionalView];
    
    [self.view addSubview:_mainScrollView];
    [self.view addSubview:_maskView];
    [self.view addSubview:_pickerView];

    
}

-(void)dataSetting
{
    /* DATA SETTING */
    NSString* currentYear = [SINGLETON getCurrentYear];
    for(int i=10;i>=0;i--)
    {
        int year = [currentYear intValue]-i;
        [_yearsArray addObject: STRING_CASCADE(INT_TO_STRING(year),@"年")];
    }
    for(int i=1;i<=31;i++)
    {
        int date = i;
        [_dateArray addObject: STRING_CASCADE(INT_TO_STRING(date),@"日")];
    }
    for(int i=0;i<=24;i++)
    {
        NSString* hour = STRING_TWO_DIGIT(i);
        [_hourArray addObject: STRING_CASCADE(hour,@"時")];
    }
    for(int i=0;i<=59;i++)
    {
        NSString* minu = STRING_TWO_DIGIT(i);
        [_miniArray addObject: STRING_CASCADE(minu,@"分")];
    }
    for(int i=5;i<=150;i++)
    {
        NSString* minu = STRING_TWO_DIGIT(i);
        [_durationArray addObject: STRING_CASCADE(minu,@"分")];
    }
    for(int i=0;i<=30;i++)
    {
        [_wateTempArray addObject:INT_TO_STRING(i)];
    }
    for(int i=3;i<=100;i++)
    {
        [_depthArray addObject: INT_TO_STRING(i)];
    }
    for(int i=0;i<=9;i++)
    {
        [_pointArray addObject: STRING_CASCADE(@".", INT_TO_STRING(i))];
    }
    
}

-(void) reloadUIrequredView
{
    /* RequiredView */
    
    for(UIView* view in [_requireView subviews]){
        [view removeFromSuperview];
    }
    
    int nextY = 0;
    
    UILabel* requireViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    [requireViewLabel setText:@"必填選項"];
    [requireViewLabel setBackgroundColor:BG_LIGHT_GRAY_COLOR];
    nextY += requireViewLabel.frame.size.height ;
    [_requireView addSubview:requireViewLabel];

    _numTextView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_numTextView.titleLabel setText:@"潛水支數(今年)"];
    [_numTextView reloadUI:INT_TO_STRING([DATABASE fetchNoOfDiveThisYear]+1) mode:REQUIRED];
    _nLog.number = [_numTextView.contentLabel.text intValue];
    nextY += _numTextView.frame.size.height;
    [_requireView addSubview:_numTextView];
    [self addAlineAtY:nextY-1 on:_requireView];
    
    _locationView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    NSString* contentStr = [NSString stringWithFormat:@"%@-%@",GET_DEFAULT(ADD_DIVE_LOCATION),GET_DEFAULT(ADD_DIVE_POINT)];
    [_locationView.titleLabel setText:@"潛水點設置"];
    [_locationView reloadUI:contentStr mode:REQUIRED];
    [_locationView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {

        MapLocationViewController* mapVC = [MapLocationViewController new];
        [mapVC setLocationSelectionCallback:^(DivePoint* divePoint) {
            _nLog.location = divePoint.location;
            _nLog.divePoint = divePoint.name;
            
            _nLog.latitude = divePoint.latitude;
            _nLog.longitude = divePoint.longitude;
            
            [DEFAULTS setObject:divePoint.name forKey:ADD_DIVE_POINT];
            [DEFAULTS setObject:divePoint.location forKey:ADD_DIVE_LOCATION];
            [DEFAULTS setObject:[NSNumber numberWithFloat:divePoint.latitude] forKey:ADD_DIVE_LATITUDE];
            [DEFAULTS setObject:[NSNumber numberWithFloat:divePoint.longitude] forKey:ADD_DIVE_LONGITUDE];
            [self reloadUIrequredView];
        }];
        [DATABASE fetchDivePointsFromPointDB:^(NSArray *locations) {
            mapVC.divePoints =[ locations mutableCopy];
            [self.navigationController pushViewController:mapVC animated:YES];
        }];
    }]];
    nextY += _locationView.frame.size.height;
    [_requireView addSubview:_locationView];
    [self addAlineAtY:nextY-1 on:_requireView];
    
    _dateView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_dateView.titleLabel setText:@"日期"];
    [_dateView reloadUI:GET_DEFAULT(ADD_DIVE_DATE) mode:REQUIRED];
    [_dateView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        int currentMonth = [[SINGLETON getCurrentMonth] intValue];
        int currentDate = [[SINGLETON getCurrentDate] intValue];
        _maskView.hidden = NO;
        [_pickerView showPickerWithThreeColumn:@[_yearsArray,MONTHES_ARRAY,_dateArray] oneOffset:[_yearsArray count]-1 twoOffset:currentMonth-1 threeOffset:currentDate-1 withCompletion:^(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex) {
            _maskView.hidden = YES;
            if(!done) return;
            NSString* str = [NSString stringWithFormat:@"%d-%@-%@",[[SINGLETON getCurrentYear] intValue]-10+selectedOneIndex,STRING_TWO_DIGIT(selectedTwoIndex+1),STRING_TWO_DIGIT( selectedThreeIndex+1)];
            _nLog.date = str;
            _nLog.datetime = [NSString stringWithFormat:@"%@ %@:00:000",_nLog.date,_nLog.beginTime];
            [DEFAULTS setObject:_nLog.date forKey:ADD_DIVE_DATE];
            [DEFAULTS setObject:_nLog.datetime forKey:ADD_DIVE_DATETIME];
            [self reloadUIrequredView];
        }];
        
    }]];
    nextY += _dateView.frame.size.height;
    [_requireView addSubview:_dateView];
    [self addAlineAtY:nextY-1 on:_requireView];

    _timeView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_timeView.titleLabel setText:@"時間"];
    [_timeView reloadUI:GET_DEFAULT(ADD_DIVE_TIME) mode:REQUIRED];
    [_timeView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        int currentHour = [[SINGLETON getCurrentHour] intValue];
        int currentMinu = [[SINGLETON getCurrentMinute] intValue];
        _maskView.hidden = NO;
        [_pickerView showPickerWithTwoColumn:@[_hourArray,_miniArray] oneOffset:currentHour twoOffset:currentMinu withCompletion:^(BOOL done, int selectedOneIndex, int selectedTwoIndex) {
            _maskView.hidden = YES;
            if(!done) return;
            NSString* time = [NSString stringWithFormat:@"%@:%@",STRING_TWO_DIGIT(selectedOneIndex),STRING_TWO_DIGIT(selectedTwoIndex) ];
            _nLog.beginTime = time;
            _nLog.datetime = [NSString stringWithFormat:@"%@ %@:00:000",_nLog.date,_nLog.beginTime];
            [DEFAULTS setObject:_nLog.beginTime forKey:ADD_DIVE_TIME];
            [DEFAULTS setObject:_nLog.datetime forKey:ADD_DIVE_DATETIME];

            [self reloadUIrequredView];
        }];
    }]];
    nextY += _timeView.frame.size.height;
    [_requireView addSubview:_timeView];
    [self addAlineAtY:nextY-1 on:_requireView];

    _durationView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_durationView.titleLabel setText:@"潛水長度"];
    if([[GET_DEFAULT(ADD_DIVE_DURATION) stringValue] isEqualToString:@"0"]){
        [_durationView reloadUI:[GET_DEFAULT(ADD_DIVE_DURATION) stringValue] mode:REQUIRED];
    }else{
        [_durationView reloadUI:[NSString stringWithFormat:@"%@分鐘",[GET_DEFAULT(ADD_DIVE_DURATION) stringValue]] mode:REQUIRED];
    }
    [_durationView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        _maskView.hidden = NO;
        [_pickerView showPickerWithOneColumn:@[_durationArray] oneOffset:[GET_DEFAULT(ADD_DIVE_DURATION) intValue]-5 withCompletion:^(BOOL done, int selectedIndex) {
            _maskView.hidden = YES;
            if(!done) return;
            _nLog.duration = selectedIndex+5;
            [DEFAULTS setObject:[NSNumber numberWithInt:_nLog.duration] forKey:ADD_DIVE_DURATION];
            [self reloadUIrequredView];
        }];
    }]];
    nextY += _durationView.frame.size.height;
    [_requireView addSubview:_durationView];
    [self addAlineAtY:nextY-1 on:_requireView];

    _temperatureView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_temperatureView.titleLabel setText:@"水溫"];
    if([[GET_DEFAULT(ADD_DIVE_TEMP) stringValue] isEqualToString:@"0"]){
        [_temperatureView reloadUI:@"" mode:REQUIRED];
    }else{
        [_temperatureView reloadUI:[NSString stringWithFormat:@"%@度",[GET_DEFAULT(ADD_DIVE_TEMP) stringValue]] mode:REQUIRED];
    }
    [_temperatureView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        _maskView.hidden = NO;
        [_pickerView showPickerWithThreeColumn:@[_wateTempArray,_pointArray,@[@"度"]] oneOffset:[GET_DEFAULT(ADD_DIVE_TEMP) intValue]  twoOffset:((int)([GET_DEFAULT(ADD_DIVE_TEMP) floatValue]*10)%10) threeOffset:0 withCompletion:^(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex) {
            _maskView.hidden = YES;
            if(!done) return;
            _nLog.temperature = (float)selectedOneIndex+selectedTwoIndex/10.0f;

            [DEFAULTS setObject:[NSNumber numberWithFloat:_nLog.temperature] forKey:ADD_DIVE_TEMP];
            [self reloadUIrequredView];
        }];
        
    }]];
    nextY += _temperatureView.frame.size.height;
    [_requireView addSubview:_temperatureView];
    [self addAlineAtY:nextY-1 on:_requireView];

    _maxDepthView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_maxDepthView.titleLabel setText:@"最大深度"];
    if([[GET_DEFAULT(ADD_DIVE_MAXDEPTH) stringValue] isEqualToString:@"0"]){
        [_maxDepthView reloadUI:@"" mode:REQUIRED];
    }else{
        [_maxDepthView reloadUI:[NSString stringWithFormat:@"%@米",[GET_DEFAULT(ADD_DIVE_MAXDEPTH) stringValue]] mode:REQUIRED];
    }
    [_maxDepthView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        _maskView.hidden = NO;
        
        [_pickerView showPickerWithThreeColumn:@[_depthArray,_pointArray,@[@"米"]] oneOffset:[GET_DEFAULT(ADD_DIVE_MAXDEPTH) intValue]-3 twoOffset:((int)([GET_DEFAULT(ADD_DIVE_MAXDEPTH) floatValue]*10)%10) threeOffset:0 withCompletion:^(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex) {
            _maskView.hidden = YES;
            if(!done) return;
            _nLog.maxDepth =  (float)selectedOneIndex+selectedTwoIndex/10.0f+3.0;
            [DEFAULTS setObject:[NSNumber numberWithFloat:_nLog.maxDepth] forKey:ADD_DIVE_MAXDEPTH];
            [self reloadUIrequredView];
        }];
    }]];
    nextY += _maxDepthView.frame.size.height;
    [_requireView addSubview:_maxDepthView];
    [self addAlineAtY:nextY-1 on:_requireView];
    
    
    
//    _avgDepthView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_avgDepthView.titleLabel setText:@"平均深度"];
//    if([[GET_DEFAULT(ADD_DIVE_AVGDEPTH) stringValue] isEqualToString:@"0"]){
//        [_avgDepthView reloadUI:@"" mode:REQUIRED];
//    }else{
//        [_avgDepthView reloadUI:[NSString stringWithFormat:@"%@米",[GET_DEFAULT(ADD_DIVE_AVGDEPTH) stringValue]] mode:REQUIRED];
//    }
//    [_avgDepthView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        [_pickerView showPickerWithThreeColumn:@[_depthArray,_pointArray,@[@"米"]] oneOffset:25 twoOffset:0 threeOffset:0 withCompletion:^(BOOL done, int selectedOneIndex, int selectedTwoIndex, int selectedThreeIndex) {
//            if(!done) return;
//            _nLog.avgDepth =  (float)selectedOneIndex+selectedTwoIndex/10.0f+3.0;
//            [DEFAULTS setObject:[NSNumber numberWithFloat:_nLog.avgDepth] forKey:ADD_DIVE_AVGDEPTH];
//            [self reloadUIrequredView];
//        }];
//    }]];
//    nextY += _avgDepthView.frame.size.height;
//    [_requireView addSubview:_avgDepthView];
//    [self addAlineAtY:nextY-1 on:_requireView];

    
    _feelView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_feelView.titleLabel setText:@"覺得"];
    [_feelView iconMode:Emotion withMode:(int)[Emotion indexOfObject:GET_DEFAULT(ADD_DIVE_FEEL)]];
    _feelView.iconView.delegate = self;
    nextY += _feelView.frame.size.height;
    [_requireView addSubview:_feelView];
    [self addAlineAtY:nextY-1 on:_requireView];
    
    
    
    
    _weatherView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_weatherView.titleLabel setText:@"天氣"];
    [_weatherView iconMode:Weather withMode:(int)[Weather indexOfObject:GET_DEFAULT(ADD_DIVE_WEATHER)]];
    _weatherView.iconView.delegate = self;
//    [_weatherView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//        // 按 Button 變狀態
//        DLog(@"ICON MODE:%d",[_weatherView getIconMode]);
//    }]];
    nextY += _weatherView.frame.size.height;
    [_requireView addSubview:_weatherView];
    [self addAlineAtY:nextY-1 on:_requireView];

    _typeView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_typeView.titleLabel setText:@"類別"];
    [_typeView iconMode:Type withMode:(int)[Type indexOfObject:GET_DEFAULT(ADD_DIVE_TYPE)]];
    _typeView.iconView.delegate = self;
    
    
//    _typeView = [[OptionalTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_typeView.titleLabel setText:@"類別"];
//    _typeView.iconView.delegate = self;
//    _typeView.iconView.viewMode = MULTI_SELECT;
//    [_typeView iconMode:Type btnTitle:@[@"船潛",@"岸潛",@"夜潛"] selected:[[NSSet alloc] initWithObjects:GET_DEFAULT(ADD_DIVE_TYPE),nil]];
    
//    for(UIView* view in [_typeView.iconView subviews]){
//        [view addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//            UIButton* btn = sender.view;
//            [btn setSelected:YES];
//            DLog(@"ICON MODE:%d",[_typeView getIconMode]);
////            UIButton* clickedBtn = sender;
////            _btnMode = (int)clickedBtn.tag;
////            DLog(@"_btnMode:%d",_btnMode);
////            [self changeMode];
//        
//        }]];
//    }

    
    nextY += _typeView.frame.size.height;
    [_requireView addSubview:_typeView];
    [self addAlineAtY:nextY-1 on:_requireView];

    [_requireView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY+5)];


}

-(void) reloadUIoptionalView
{
    /* OptionView */
    int nextY = 0;
    _optionalView = [[UIScrollView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)];
    [_optionalView setBackgroundColor:WHITE_COLOR];
    
    _optionViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    [_optionViewLabel setText:@"選填選項"];
    [_optionViewLabel setBackgroundColor:BG_LIGHT_GRAY_COLOR];
    nextY += _optionViewLabel.frame.size.height ;
    [_optionalView addSubview:_optionViewLabel];

    _imageLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, nextY+5, 100, 30)];
    [_imageLabel setText:@"圖片"];
    _logImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"album_unlock"]];
    _logImageView.frame =CGRectMake(SCREEN_WIDTH-90, nextY+5, 80, 80);
    _logImageView.layer.cornerRadius = 10.0f;
    _logImageView.layer.masksToBounds = YES;
    _logImageView.contentMode = UIViewContentModeScaleAspectFill;
    UITapGestureRecognizer* tapGesture= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addPicture:)];
    [_logImageView addGestureRecognizer:tapGesture];
    _logImageView.userInteractionEnabled = YES;
    
    
    [_optionalView addSubview:_imageLabel];
    [_optionalView addSubview:_logImageView];
    nextY += 95;
    [self addAlineAtY:nextY-1 on:_optionalView];

    _portTypeView = [[OptionalTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_portTypeView.titleLabel setText:@"景觀"];
    _portTypeView.iconView.delegate = self;
    _portTypeView.iconView.viewMode = MULTI_SELECT;
    [_portTypeView iconMode:@[@"huge_rain",@"rain",@"rain",@"rain",@"rain"] btnTitle:@[@"沈船",@"峭壁",@"沙地",@"洞穴",@"礁石"] selected:[[NSSet alloc] initWithObjects:@"",nil]];
    [_portTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    }]];
    nextY += _portTypeView.frame.size.height;
    [_optionalView addSubview:_portTypeView];
    [self addAlineAtY:nextY-1 on:_optionalView];

    _airTypeView = [[OptionalTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_airTypeView.titleLabel setText:@"氣瓶"];
    _airTypeView.iconView.delegate = self;
    _airTypeView.iconView.viewMode = SINGLE_SELECT;
    [_airTypeView iconMode:@[@"huge_rain",@"rain",@"rain"] btnTitle:@[@"空氣",@"高氧",@"高氧"] selected:[NSSet new]];
    [_airTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    }]];
    nextY += _airTypeView.frame.size.height;
    [_optionalView addSubview:_airTypeView];
    [self addAlineAtY:nextY-1 on:_optionalView];

    _suitTypeView = [[OptionalTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_suitTypeView.titleLabel setText:@"防寒衣"];
    _suitTypeView.iconView.delegate = self;
    _suitTypeView.iconView.viewMode = SINGLE_SELECT;
    [_suitTypeView iconMode:@[@"huge_rain",@"rain",@"rain"] btnTitle:@[@"3mm",@"5mm",@"乾式"] selected:[NSSet new]];
    [_suitTypeView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    }]];
    nextY += _suitTypeView.frame.size.height;
    [_optionalView addSubview:_suitTypeView];
    [self addAlineAtY:nextY-1 on:_optionalView];

//    _specialView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_specialView.titleLabel setText:@"特別的"];
//    [_specialView.contentLabel setText:@"選填"];
//    [_specialView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
//    nextY += _specialView.frame.size.height;
    
    _creatureView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_creatureView.titleLabel setText:@"生物"];
    _creatureView.iconView.viewMode = MULTI_SELECT;
    [_creatureView iconMode:@[@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain",@"rain"] withMode:0];
    _creatureView.iconView.delegate = self;
    
    
//    _creatureView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_creatureView.titleLabel setText:@"生物"];
//    [_creatureView.contentLabel setText:@"選填"];
//    [_creatureView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
    
    nextY += _creatureView.frame.size.height;
    [_optionalView addSubview:_creatureView];
    [self addAlineAtY:nextY-1 on:_optionalView];

    _partnerView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_partnerView.titleLabel setText:@"潛水夥伴"];
    [_partnerView reloadUI:@"" mode:OPTIONAL];
    [_partnerView.arrow setHidden:NO];
    [_partnerView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    }]];
    nextY += _partnerView.frame.size.height;
    [_optionalView addSubview:_partnerView];
    [self addAlineAtY:nextY-1 on:_optionalView];

    _noteView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
    [_noteView.titleLabel setText:@"筆記"];
    [_noteView reloadUI:@"" mode:OPTIONAL];
    [_noteView.arrow setHidden:NO];
    [_noteView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
    }]];
    nextY += _noteView.frame.size.height;
    [_optionalView addSubview:_noteView];
    [self addAlineAtY:nextY-1 on:_optionalView];

//    _imageView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_imageView.titleLabel setText:@"圖片"];
//    [_imageView reloadUI:@"" mode:OPTIONAL];
//    [_imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
//    nextY += _partnerView.frame.size.height;
//    [_optionalView addSubview:_imageView];
//    [self addAlineAtY:nextY-1 on:_optionalView];

    [_optionalView setContentSize:CGSizeMake(SCREEN_WIDTH, nextY)];
    
//    _curveView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_curveView.titleLabel setText:@"曲線"];
//    [_curveView.contentLabel setText:@"選填"];
//    [_curveView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
//    nextY += _partnerView.frame.size.height;
//    
//    _tideView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_tideView.titleLabel setText:@"潮汐"];
//    [_tideView.contentLabel setText:@"選填"];
//    [_tideView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
//    nextY += _tideView.frame.size.height;
//    
//    _mapView = [[CreateTextView alloc]initWithFrame:CGRectMake(0, nextY, SCREEN_WIDTH, 40)];
//    [_mapView.titleLabel setText:@"地圖"];
//    [_mapView.contentLabel setText:@"選填"];
//    [_mapView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
//    }]];
//    nextY += _mapView.frame.size.height;
    
    
//    [_optionalView addSubview:_curveView];
//    [_optionalView addSubview:_tideView];
//    [_optionalView addSubview:_mapView];
    
}

-(void)doneAction:(id)sender
{
    [_nLog checkCompletion:^(BOOL success, NSString *message) {
        if(!success){
            DLog(@"%@",message);
            return;
        }
        else{
            
            
            [DATABASE replaceInfoLog:[_nLog getDicWithLogObject]];
////            divePoint.pictures = TO_JSON(@[_nLog.picture]);
////            divePoint.creatures = TO_JSON(@[_nLog.creatures]);
            [self.navigationController popViewControllerAnimated:YES];
            [DEFAULTS synchronize];
            
            if(![_nLog.picture isEqualToString:@""]){
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library addAssetURL: STRING_TO_URL(_nLog.picture) toAlbum:@"DiveLog0" withCompletionBlock:^(NSError *error) {
                    if(!error){
                        DLog(@"Save image to DiveLog");
                    }
                }];
            }
            
            [_delegate didAddNewLog:_nLog];

        }
    }];
}


-(void) hidePicker:(id)sender {
    [_pickerView hidePicker];
    _maskView.hidden = YES;
}

-(void)addAlineAtY:(int)height on:(UIView*)view{
    UIView* lineView = [[UIView alloc]initWithFrame:CGRectMake(0, height-1, SCREEN_WIDTH, 0.5)];
    [lineView setBackgroundColor:[UIColor darkGrayColor]];
    [view addSubview:lineView];
}


#pragma mark IconDelegate
-(void) didClickIcon:(int)btnMode onView:(UIView *)view
{
    if([view isEqual:_weatherView.iconView]){
        DLog(@"Weather:%@",Weather[btnMode]);
        _nLog.weather = Weather[btnMode];
        [DEFAULTS setObject:Weather[btnMode] forKey:ADD_DIVE_WEATHER];
    }else if([view isEqual:_typeView.iconView]){
        DLog(@"Type:%@",Type[btnMode]);
        _nLog.type = Type[btnMode];
        [DEFAULTS setObject:Type[btnMode] forKey:ADD_DIVE_TYPE];
    }else if([view isEqual:_feelView.iconView]){
        DLog(@"Feel:%@",Emotion[btnMode]);
        _nLog.feel = Emotion[btnMode];
        [DEFAULTS setObject:Emotion[btnMode] forKey:ADD_DIVE_FEEL];
    }else if([view isEqual:_creatureView.iconView]){
        // 生物 多選
        
    }
    
}

#pragma mark MultiIconDelegate
-(void)didClickMultiIcon:(int)btnMode onView:(UIView*)view
{
    if([view isEqual:_portTypeView.iconView]){
        // 景觀 多選
        if([_selectedPort containsObject:Scenery[btnMode]]){
            [_selectedPort removeObject:Scenery[btnMode]];
        }else{
            [_selectedPort addObject:Scenery[btnMode]];
        }
        _nLog.port = TO_JSON([_selectedPort allObjects]);
        DLog(@"_portTypeView:%@", _nLog.port);

    }else if([view isEqual:_airTypeView.iconView]){
        // 氣瓶 單選
        if([_nLog.air isEqualToString:Air[btnMode]]){
            _nLog.air  = @"";
        }else{
            _nLog.air = Air[btnMode];
        }
        DLog(@"_airTypeView:%@", _nLog.air);

    }else if([view isEqual:_suitTypeView.iconView]){
        // 防寒衣 單選
        if([_nLog.suit isEqualToString:Suits[btnMode]]){
            _nLog.suit  = @"";
        }else{
            _nLog.suit = Suits[btnMode];
        }
        DLog(@"_suitTypeView:%@", _nLog.suit);
    }

}


#pragma mark ImagePickrAddPicture
-(void) addPicture:(id)sender
{
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = (id) self;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker animated:YES completion:^{
    }];
    
}

#pragma mark UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    
    _logImageView.image = selectedImage;
    NSURL *imageurl  = [info objectForKey: UIImagePickerControllerReferenceURL];
    _nLog.picture = [imageurl  absoluteString];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:NO];
}


@end
