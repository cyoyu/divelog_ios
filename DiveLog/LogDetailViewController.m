//
//  LogDetailViewController.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "LogDetailViewController.h"

@implementation LogDetailViewController


- (id)init
{
    self = [super init];
    if (self != nil){
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    
    _mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    
    _locationLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 30)];
    _divePointLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0,40, SCREEN_WIDTH, 30)];
    _timeStampLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 70, SCREEN_WIDTH, 30)];
    _dateLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, 30)];
    _durationLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 130, SCREEN_WIDTH, 30)];
    _temperatureLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 160, SCREEN_WIDTH, 30)];
    _maxDepthLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 190, SCREEN_WIDTH, 30)];
    _typeLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 220, SCREEN_WIDTH, 30)];
    _feelLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 250, SCREEN_WIDTH, 30)];
    _weatherLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 280, SCREEN_WIDTH, 30)];

    _nitroxLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 310, SCREEN_WIDTH, 30)];
    _suitLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 340, SCREEN_WIDTH, 30)];
    _portLabel = [[DoubleLabelView alloc]initWithFrame:CGRectMake(0, 370, SCREEN_WIDTH, 30)];

    
    
    _diveImageView = [[UIImageView alloc]initWithFrame:CGRectMake(100, 50, 90, 90)];
    _diveImageView.contentMode = UIViewContentModeScaleAspectFill;
    [_diveImageView setBackgroundColor:MAIN_COLOR];
    
//    _diveImageView.layer.cornerRadius = 10.0f;
    _diveImageView.layer.masksToBounds = YES;
    
//    _dive
//    
//    @property (nonatomic, strong) UIImageView* diveImageView;
//    @property (nonatomic, strong) UILabel* nitroxLabel;
//    @property (nonatomic, strong) UILabel* suitLabel;
//    @property (nonatomic, strong) UILabel* specialLabel;
//    @property (nonatomic, strong) UILabel* creatureLabel;
//    @property (nonatomic, strong) UILabel* noteLabel;
//    @property (nonatomic, strong) UILabel* imageLabel;
//    @property (nonatomic, strong) UILabel* curveLabel;
//    @property (nonatomic, strong) UILabel* partnerLabel;
    
    
    [self.view addSubview:_mainScrollView];

    [_mainScrollView addSubview:_locationLabel];
    [_mainScrollView addSubview:_divePointLabel];
    [_mainScrollView addSubview:_timeStampLabel];
    [_mainScrollView addSubview:_dateLabel];
    [_mainScrollView addSubview:_temperatureLabel];
    [_mainScrollView addSubview:_maxDepthLabel];
    [_mainScrollView addSubview:_durationLabel];
    [_mainScrollView addSubview:_typeLabel];
    [_mainScrollView addSubview:_feelLabel];
    [_mainScrollView addSubview:_weatherLabel];
    
    [_mainScrollView addSubview:_diveImageView];

    [_mainScrollView addSubview:_nitroxLabel];
    [_mainScrollView addSubview:_suitLabel];
    [_mainScrollView addSubview:_portLabel];

    
    UIScreenEdgePanGestureRecognizer *popRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePopRecognizer:)];
    popRecognizer.edges = UIRectEdgeLeft;
    [_mainScrollView addGestureRecognizer:popRecognizer];
    _mainScrollView.userInteractionEnabled = YES;
    
    [self reloadUI];
    
    
}

-(void) reloadUI
{
    DLog(@"picture:%@",_logObject.picture);
    if(![_logObject.picture isEqualToString:@""]){
        [IMAGE_MANAGER setImageWithAssetURL:_diveImageView assetURL:_logObject.picture ];
    }
    
    DLog(@"123123");
    
    [_locationLabel.titleLabel setText:@"地點:"];
    [_locationLabel.contentLabel setText:_logObject.location];
    [_divePointLabel.titleLabel setText:@"潛點名稱:"];
    [_divePointLabel.contentLabel setText:_logObject.divePoint];
    [_timeStampLabel.titleLabel setText:@"時間:"];
    [_timeStampLabel.contentLabel setText:_logObject.beginTime];
    [_dateLabel.titleLabel setText:@"日期:"];
    [_dateLabel.contentLabel setText:_logObject.date];
    [_durationLabel.titleLabel setText:@"長度:"];
    [_durationLabel.contentLabel setText:INT_TO_STRING(_logObject.duration)];
    [_temperatureLabel.titleLabel setText:@"水溫:"];
    [_temperatureLabel.contentLabel setText:FLOAT_TO_STRING(_logObject.temperature)];
    [_maxDepthLabel.titleLabel setText:@"最大深度:"];
    [_maxDepthLabel.contentLabel setText:FLOAT_TO_STRING(_logObject.maxDepth)];
    [_typeLabel.titleLabel setText:@"類型:"];
    [_typeLabel.contentLabel setText:_logObject.type];
    [_feelLabel.titleLabel setText:@"感覺:"];
    [_feelLabel.contentLabel setText:_logObject.feel];
    [_weatherLabel.titleLabel setText:@"天氣:"];
    [_weatherLabel.contentLabel setText:_logObject.weather];

    
    DLog(@"air:%@",_logObject.air);
    if(![_logObject.air isEqualToString:@""]){
        [_nitroxLabel.titleLabel setText:@"氣瓶:"];
        [_nitroxLabel.contentLabel setText:_logObject.air];
    }
    
    [_portLabel.titleLabel setText:@"景觀:"];
    [_portLabel.contentLabel setText:_logObject.port];
    [_suitLabel.titleLabel setText:@"防寒衣:"];
    [_suitLabel.contentLabel setText:_logObject.suit];
    
}

#pragma mark - <YCZoomTransitionAnimating>
- (UIImageView *)transitionSourceImageView
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.diveImageView.image];
    imageView.contentMode = self.diveImageView.contentMode;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled = NO;
    imageView.frame = self.diveImageView.frame;
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return self.view.backgroundColor;
}

- (CGRect)transitionDestinationImageViewFrame
{
    return self.diveImageView.frame;
}



- (void)handlePopRecognizer:(UIScreenEdgePanGestureRecognizer*)recognizer {
    // Calculate how far the user has dragged across the view
    CGFloat progress = [recognizer translationInView:self.view].x / (self.view.bounds.size.width * 1.0);
    progress = MIN(1.0, MAX(0.0, progress));
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // Create a interactive transition and pop the view controller
        self.interactivePopTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        // Update the interactive transition's progress
        [self.interactivePopTransition updateInteractiveTransition:progress];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        // Finish or cancel the interactive transition
        if (progress > 0.5) {
            [self.interactivePopTransition finishInteractiveTransition];
        }
        else {
            [self.interactivePopTransition cancelInteractiveTransition];
        }
        
        self.interactivePopTransition = nil;
    }
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController
                         interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
    // Check if this is for our custom transition
    if ([animationController isKindOfClass:[YCZoomTransitionAnimator class]]) {
        return self.interactivePopTransition;
    }
    else {
        return nil;
    }
}

@end
