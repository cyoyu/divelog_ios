//
//  CreateTextView.m
//  DiveLog
//
//  Created by York on 12/19/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "OptionalTextView.h"

@implementation OptionalTextView

@synthesize titleLabel;
@synthesize viewMode;
@synthesize iconView;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    viewMode = TEXT_MODE;
    
    titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(10, frame.size.height/2-20, frame.size.width/2-10, 40);
    [titleLabel setText:@"標題"];
   
    
    iconView = [[MultiIconView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 5, 0, 0)];
    [self addSubview:iconView];
    
    
    [self setBackgroundColor:[UIColor whiteColor]];
    
    [self addSubview:titleLabel];
    
    return self;
}

//-(void) changeMode:(int)mode
//{
//    viewMode = mode;
//    
//    if(viewMode==TEXT_MODE){
//        
//    }else{
//      
//    }
//    return ;
//}

-(float) heightForCell
{
    return self.frame.size.height;
}



-(void)iconMode:(NSArray*) imgArray btnTitle:(NSArray*)titles selected:(NSSet*)sets
{
    iconView.hidden = NO;
    [iconView reloadUIwithImages:imgArray btnTitle:titles selected:sets];
    if(iconView.frame.size.height>self.frame.size.height){
        self.frame = CGRectMake(self.frame.origin.x,self.frame.origin.y, SCREEN_WIDTH, iconView.frame.size.height+10);
    }
}




@end
