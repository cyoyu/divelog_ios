//
//  LogTableViewController.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "LogTableViewController.h"
#import "LogDetailViewController.h"
#import "Constant.h"

#define cellIdentifier @"TableCell"


@implementation LogTableViewController

#define TABLE_MODE @"table_mode"
#define CALENDER_MODE @"calendar_mode"

- (id)init
{
    self = [super init];
    if (self != nil){
        _mode = TABLE_MODE;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    
    DLog(@"HELLO");
    
    UIButton* modeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [modeBtn setBackgroundImage:[UIImage imageNamed:@"literature-50"] forState:UIControlStateNormal];
    [modeBtn setBackgroundImage:[UIImage imageNamed:@"literature-50"] forState:UIControlStateHighlighted];
    [modeBtn addTarget:self action:@selector(didClickMode:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]  initWithCustomView:modeBtn];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addLogInfo:)];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    _logTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0, SCREEN_WIDTH, SCREEN_HEIGHT-TAB_BAR_HEIGHT-STATUS_BAR_HEIGHT-NAVI_BAR_HEIGHT)];
    
    [_logTableView registerClass:[LogCell class] forCellReuseIdentifier:cellIdentifier];
    _logTableView.delegate = self;
    _logTableView.dataSource = self;
    
    _noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (SCREEN_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)/2-60, SCREEN_WIDTH, 30)];
    [_noDataLabel setText:@"Let's Go Diving Right Now!"];
    [_noDataLabel setTextColor:[UIColor grayColor]];
    _noDataLabel.textAlignment = NSTextAlignmentCenter;

    _noDataLabel.hidden = YES;
    
    [self.view addSubview:_logTableView];
    [self.view addSubview:_noDataLabel];
    
    [DATABASE fetchLogInfoWithCompletion:^(BOOL success, NSArray *logObjects) {
        if(success){
            
            if([logObjects count]==0){
                _logTableView.hidden = YES;
                _noDataLabel.hidden = NO;
            }
            
            _logs = [logObjects mutableCopy];
            [_logTableView reloadData];
        }
    }];
    
}


# pragma mark UITableViewDateSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_logs count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    LogCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil){
        cell = [[LogCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.delegate = self;
    int index = [_logs count]-indexPath.row-1;
    [cell reloadUI:_logs[index]];
    cell.selectionStyle = UITableViewCellAccessoryNone;
    return cell;
}


# pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LogDetailViewController* ldVC = [LogDetailViewController new];
    ldVC.hidesBottomBarWhenPushed = YES;
    int index = [_logs count]-indexPath.row-1;
    ldVC.logObject = _logs[index];
    [self.navigationController pushViewController:ldVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        LogObject* log = [_logs objectAtIndex:[_logs count]-indexPath.row-1];
        [_logs removeObject:log];
        [DATABASE deleteLogWithLogObject:log];
        [_logTableView reloadData];
    }
}



# pragma mark - TableViewCellDelegate
-(void)didClickSomething:(id)sender
{
}

-(void)addLogInfo:(id)sender
{
    // go to add log view controller
    NewLogViewController* nVC = [NewLogViewController new];
    nVC.hidesBottomBarWhenPushed = YES;
    nVC.delegate = self;
    [self.navigationController pushViewController:nVC animated:YES];
    
}

#pragma mark NewLogDelegate
-(void) didAddNewLog:(LogObject*)logObject;
{
    [_logs removeAllObjects];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [DATABASE fetchLogInfoWithCompletion:^(BOOL success, NSArray *logObjects) {
            if(success){
                if([logObjects count]==0){
                    _logTableView.hidden = YES;
                    _noDataLabel.hidden = NO;
                }else{
                    _logTableView.hidden = NO;
                    _noDataLabel.hidden = YES;
                }
                
                _logs = [logObjects mutableCopy];
                [_logTableView reloadData];
            }
        }];
    });
    //    [DATABASE fetchLogInfoWithCompletion:^(BOOL success, NSArray *logObjects) {
    //        if(success){
    //            _logs = [logObjects mutableCopy];
    //            [_logTableView reloadData];
    //        }
    //    }];
}

-(void)didClickMode:(id)sender
{
    if([_mode isEqualToString:TABLE_MODE]){
        _mode = CALENDER_MODE;
        UIButton* modeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [modeBtn setBackgroundImage:[UIImage imageNamed:@"date_to-50"] forState:UIControlStateNormal];
        [modeBtn setBackgroundImage:[UIImage imageNamed:@"date_to-50"] forState:UIControlStateHighlighted];
        [modeBtn addTarget:self action:@selector(didClickMode:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]  initWithCustomView:modeBtn];
        
        
    }else{
        _mode = TABLE_MODE;
        UIButton* modeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [modeBtn setBackgroundImage:[UIImage imageNamed:@"literature-50"] forState:UIControlStateNormal];
        [modeBtn setBackgroundImage:[UIImage imageNamed:@"literature-50"] forState:UIControlStateHighlighted];
        [modeBtn addTarget:self action:@selector(didClickMode:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]  initWithCustomView:modeBtn];
        
    }
    
}





//#pragma mark <RMPZoomTransitionAnimating>
//- (UIImageView *)transitionSourceImageView
//{
//    NSIndexPath *selectedIndexPath = [self.logTableView indexPathForSelectedRow];
//    LogCell *cell = (LogCell *)[self.logTableView cellForRowAtIndexPath:selectedIndexPath];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:cell.mainImageView.image];
//    imageView.contentMode = cell.mainImageView.contentMode;
//    imageView.clipsToBounds = YES;
//    imageView.userInteractionEnabled = NO;
//    CGRect frameInSuperview = [cell.mainImageView convertRect:cell.mainImageView.frame toView:self.logTableView.superview];
//    frameInSuperview.origin.x -= [cell.mainImageView layoutMargins].left;
//    frameInSuperview.origin.y -= [cell.mainImageView layoutMargins].top;
//    imageView.frame = frameInSuperview;
//    return imageView;
//}
//
//- (UIColor *)transitionSourceBackgroundColor
//{
//    return self.logTableView.backgroundColor;
//}
//
//- (CGRect)transitionDestinationImageViewFrame
//{
//    NSIndexPath *selectedIndexPath = [self.logTableView indexPathForSelectedRow];
//    LogCell *cell = (LogCell *)[self.logTableView cellForRowAtIndexPath:selectedIndexPath];
//    CGRect frameInSuperview = [cell.mainImageView convertRect:cell.mainImageView.frame toView:self.logTableView.superview];
//    frameInSuperview.origin.x -= [cell.mainImageView layoutMargins].left;
//    frameInSuperview.origin.y -= [cell.mainImageView layoutMargins].top;
//    return frameInSuperview;
//}


- (UIImageView *)transitionSourceImageView
{
    NSIndexPath *selectedIndexPath = [self.logTableView indexPathForSelectedRow];
    LogCell *cell = (LogCell *)[self.logTableView cellForRowAtIndexPath:selectedIndexPath];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:cell.mainImageView.image];
    imageView.contentMode = cell.mainImageView.contentMode;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled = NO;
    CGRect frameInSuperview = [cell.mainImageView convertRect:cell.mainImageView.frame toView:self.logTableView.superview];
    frameInSuperview.origin.x -= [cell.mainImageView layoutMargins].left;
    frameInSuperview.origin.y -= [cell.mainImageView layoutMargins].top;
    imageView.frame = frameInSuperview;
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return self.logTableView.backgroundColor;
}

- (CGRect)transitionDestinationImageViewFrame
{
    NSIndexPath *selectedIndexPath = [self.logTableView indexPathForSelectedRow];
    LogCell *cell = (LogCell *)[self.logTableView cellForRowAtIndexPath:selectedIndexPath];
    CGRect frameInSuperview = [cell.mainImageView convertRect:cell.mainImageView.frame toView:self.logTableView.superview];
    frameInSuperview.origin.x -= [cell.mainImageView layoutMargins].left;
    frameInSuperview.origin.y -= [cell.mainImageView layoutMargins].top;
    return frameInSuperview;
}


#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    id <YCZoomTransitionAnimating> sourceTransition = (id<YCZoomTransitionAnimating>)source;
    id <YCZoomTransitionAnimating> destinationTransition = (id<YCZoomTransitionAnimating>)presented;
    if ([sourceTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)]) {
        YCZoomTransitionAnimator *animator = [[YCZoomTransitionAnimator alloc] init];
        animator.goingForward = YES;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    id <YCZoomTransitionAnimating> sourceTransition = (id<YCZoomTransitionAnimating>)dismissed;
    id <YCZoomTransitionAnimating> destinationTransition = (id<YCZoomTransitionAnimating>)self;
    if ([sourceTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)]) {
        YCZoomTransitionAnimator *animator = [[YCZoomTransitionAnimator alloc] init];
        animator.goingForward = NO;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}





@end
