//
//  YCNavigationController.m
//  DiveLog
//
//  Created by York on 4/23/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "YCNavigationController.h"
#import "YCZoomTransitionAnimator.h"


@implementation YCNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIPanGestureRecognizer* panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    [self.view addGestureRecognizer:panRecognizer];
    
    
    self.delegate = self;
}

- (void)pan:(UIPanGestureRecognizer*)recognizer
{
    UIView* view = self.view;
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint location = [recognizer locationInView:view];
        if (location.x > CGRectGetMidX(view.bounds) && self.viewControllers.count == 1){
            self.interactionController = [UIPercentDrivenInteractiveTransition new];
            [self popViewControllerAnimated:YES];
      //      [self pushViewController:self.visibleViewController animated:NO];
//            [self.navigationController.visibleViewController performSegueWithIdentifier:@"" sender:self];
        }
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:view];
        // fabs() 求浮点数的绝对值
        CGFloat d = fabs(translation.x / CGRectGetWidth(view.bounds));
        [self.interactionController updateInteractiveTransition:d];
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        // 手勢結束
        
        if ([recognizer velocityInView:view].x < 0) {
            [self.interactionController finishInteractiveTransition];
        } else {
            [self.interactionController cancelInteractiveTransition];
        }
        self.interactionController = nil;
    }
}



#pragma mark - <UINavigationControllerDelegate>
// 回傳 AnimatedTransitioning 
- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    
    id <YCZoomTransitionAnimating> sourceTransition = (id<YCZoomTransitionAnimating>)fromVC;
    id <YCZoomTransitionAnimating> destinationTransition = (id<YCZoomTransitionAnimating>)toVC;
    if ([sourceTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(YCZoomTransitionAnimating)]) {
        YCZoomTransitionAnimator *animator = [[YCZoomTransitionAnimator alloc] init];
        animator.goingForward = (operation == UINavigationControllerOperationPush);
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
       
        if (operation == UINavigationControllerOperationPush) {
            // push called
            return animator;
        }
        
        return animator;
    }
    return nil;
}

- (id <UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController*)navigationController
                          interactionControllerForAnimationController:(id <UIViewControllerAnimatedTransitioning>)animationController
{
    return self.interactionController;
}



@end
