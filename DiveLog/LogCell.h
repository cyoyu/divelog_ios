//
//  LogCell.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogObject.h"

@protocol LogCellDelegate <NSObject>
-(void)didClickSomething:(id)sender;
@end

@interface LogCell : UITableViewCell

@property (assign) id<LogCellDelegate> delegate;
//@property (nonatomic,strong) LogObject* logObject;

@property (nonatomic,strong) UILabel* numberOfDive;
@property (nonatomic,strong) UILabel* locationLabel;
@property (nonatomic,strong) UILabel* divePointLabel;
@property (nonatomic,strong) UILabel* timeStampLabel;
@property (nonatomic,strong) UILabel* dateLabel;
@property (nonatomic,strong) UILabel* durationLabel;
@property (nonatomic,strong) UILabel* temperatureLabel;
@property (nonatomic,strong) UILabel* maxDepthLabel;
@property (nonatomic,strong) UIImageView* typeImageView;
@property (nonatomic,strong) UIImageView* feelImageView;
@property (nonatomic,strong) UIImageView* weatherImageView;


@property (nonatomic,strong) UIImageView* mainImageView;

-(void) reloadUI:(LogObject*)logObject;

@end