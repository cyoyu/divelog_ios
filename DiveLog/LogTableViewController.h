//
//  LogTableViewController.h
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LogCell.h"
#import "Constant.h"
#import "NewLogViewController.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "YCZoomTransitionAnimator.h"

@interface LogTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,LogCellDelegate,NewLogDelegate,YCZoomTransitionAnimating>


@property(nonatomic,strong) UIScrollView* calendarView;
@property(nonatomic,strong) UITableView* logTableView;
@property(nonatomic,strong) UILabel* noDataLabel;
@property(nonatomic,strong) NSMutableArray* logs;
@property(nonatomic,strong) NSString* mode;


@end
