//
//  DoubleLabelView.h
//  DiveLog
//
//  Created by York on 1/25/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoubleLabelView : UIView

@property (nonatomic,strong) UILabel* titleLabel;
@property (nonatomic,strong) UILabel* contentLabel;


@end
