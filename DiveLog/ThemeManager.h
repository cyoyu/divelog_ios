//
//  ThemeManager.h
//  Gay
//
//  Created by tpy on 2014/5/5.
//  Copyright (c) 2014年 POPO_INNOVATION. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"

@interface ThemeManager : NSObject

+(UILabel*) newLogLabel;
+(UIScrollView*) bouncedPageScrollView;
+(UITextField*) newTextField;



+(UIColor *)mainColor;
+(UIColor *)mainHighlightColor;
+(UIColor *)lightGrayBgColor;
+(UIColor *)unreadCellColor;
+(UIButton*)plainButton;
+(UIButton *)blueButton;
+(UIButton *)greenButton;
+(UIButton*)badgeButton;
+(UIColor *)backgroundColor;
+(UIColor *)userTableViewCellNameLabelColor;
+(UIColor *)userTableViewCellInfoLabelColor;
+(UIColor *)userTableViewCellTimestampColor;
+(UIColor *)userTableViewCellSignatureColor;
+(UIColor *)navTitleColor;
+(UIColor *)navRightItemTextColor;
+(UIColor *)navbarBackgroundColor;
+(UIButton *)navBackButton;
+(UIButton *)searchIconButton;
+(UIButton *)gridIconButton;
+(UIButton *)listIconButton;
+(UIButton *)writePostButton;
+(UIColor *)nearbyViewBackgroundColor;
+(UIColor *)focusViewBackgroundColor;
+(UIButton *)interestButton;
+(UIButton *)notInterestButton;
+(UIButton *)chatButton;
+(UIButton *)giftButton;
+(UIButton *)likeButton;
+(UIButton *)commentButton;
+(UIButton *)addPhotoButton;
+(UIImageView *)postCellBackgroundView;
+(UIImageView *)postMaintabBackgroundImageView;
+(UITextField *)styledTextField;
+(UIColor *)postCellHeaderInfoLabelColor;
+(UIColor *)metaCountColor;
+(UIImageView*)disclosureImageView;
+(void)addBottomSeperatorLineOnView:(UIView*)superView;
@end


