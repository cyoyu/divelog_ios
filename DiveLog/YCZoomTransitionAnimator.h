//
//  YCZoomTransitionAnimator.h
//  DiveLog
//
//  Created by York on 4/23/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol YCZoomTransitionAnimating <NSObject>
@required
// @return source view controller's UIImageView before transition.
- (UIImageView *)transitionSourceImageView;
//@return source view controller's bacground color
- (UIColor *)transitionSourceBackgroundColor;
//@return destination view controller's frame for UIImageView
- (CGRect)transitionDestinationImageViewFrame;
@end

@interface YCZoomTransitionAnimator : NSObject <UIViewControllerAnimatedTransitioning>
@property (nonatomic) BOOL goingForward;
@property (nonatomic, weak) id <YCZoomTransitionAnimating> sourceTransition;
@property (nonatomic, weak) id <YCZoomTransitionAnimating> destinationTransition;

@end
