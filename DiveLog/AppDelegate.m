//
//  AppDelegate.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "AppDelegate.h"
#import "LogTableViewController.h"
#import "SettingViewController.h"
#import "MainViewController.h"
#import "Constant.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//-(void) customizeInterface
//{
//    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//                                               [UIColor whiteColor],UITextAttributeTextColor,
//                                               [UIColor clearColor], UITextAttributeTextShadowColor,
//                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
//
//    [[UINavigationBar appearance] setBarTintColor:NAVY];
//    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
//    [[UINavigationBar appearance] setTintColor:[UIColor clearColor]];
//
//    [[UITabBar appearance] setBarTintColor:LIGHT_GRAY_COLOR];
//    [[UITabBar appearance] setTintColor:LIGHT_GRAY_COLOR];
//
//    [[UIBarButtonItem appearance] setTintColor:WHITE_COLOR];
//
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    // [self customizeInterface];
    
    [Singleton sharedInstance];
    [SINGLETON customizeInterface];
    
    
    // Link to Parse server
    //    [Parse setApplicationId:@"pS0LiESD0TDl1oJqkLGm0AwQW94Qu7Ivz8aSWFhl"
    //                  clientKey:@"Nre3At3lrECCKUXySpZPTweDhl001dSfN6AYArKN"];
    //    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    [SINGLETON setup];
    [self.window setRootViewController:[SINGLETON mainTabBarViewController] ];
    
    [self initializeDefaults];
    return YES;
}






-(void) initializeDefaults
{
    [DEFAULTS registerDefaults:@{
                                 NAME:@"",
                                 BIRTHDAY:@"",
                                 GENDER:@"",
                                 LINCENSE:@"",
                                 SYSTEM:@"",
                                 
                                 TOTAL_DIVE:@0,
                                 LAST_DIVE_DATE:@0,
                                 LAST_DIVE_TIME:@0,
                                 LAST_TEMP:@"",
                                 LAST_WEATHER:@"",
                                 LAST_TYPE:@"",
                                 LAST_FELL:@[],
                                 LAST_MAIN_TAB:@0,
                                 TOTAL_DIVE_THIS_YEAR:@0,
                                 
                                 ADD_DIVE_NUM:@0,
                                 ADD_DIVE_LOCATION:@"",
                                 ADD_DIVE_POINT:@"",
                                 ADD_DIVE_LATITUDE:@0,
                                 ADD_DIVE_LONGITUDE:@0,
                                 ADD_DIVE_DATE:@"",
                                 ADD_DIVE_TIME:@"",
                                 ADD_DIVE_DURATION:@0,
                                 ADD_DIVE_TEMP:@0.0,
                                 ADD_DIVE_MAXDEPTH:@0.0,
                                 ADD_DIVE_AVGDEPTH:@0.0,
                                 ADD_DIVE_WEATHER:@"",
                                 ADD_DIVE_FEEL:@"",
                                 ADD_DIVE_TYPE:@"",
                                 ADD_DIVE_DATETIME:@"",
                                 ADD_DIVE_COLOR:WHITE
                                 }];
    [DEFAULTS synchronize];
}







- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
