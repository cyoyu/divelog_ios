//
//  MainViewController.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "MainViewController.h"
#import "EditProfileViewController.h"

@implementation MainViewController


#define headerHeight (SCREEN_HEIGHT-TAB_BAR_HEIGHT-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT)/5
#define leftMargin 15
#define topMargin 10




- (id)init
{
    self = [super init];
    if (self != nil){
    }
    return self;
}


-(void)viewDidLoad
{
    [self configureViewForIOS7];
//    self.title = @"Home";
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:0.1f]];

     _library = [[ALAssetsLibrary alloc] init];

    // HeaderView
    _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, headerHeight)];
    [_headerView setBackgroundColor:BG_LIGHT_GRAY_COLOR];
    
    _userName = [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, 5, 100, 30)];
    [_userName setText:@"York Chou"];
    [_userName setFont:SYSTEM_FONT_WITH_SIZE(14)];
    _userName.numberOfLines = 1;
    [_userName sizeToFit];

    
    _userImageView = [UIImageView new];
    _userImageView.frame = CGRectMake(3*SCREEN_WIDTH/10, 30, SCREEN_WIDTH/2, SCREEN_WIDTH/2);
    _userImageView.layer.cornerRadius = SCREEN_WIDTH/5;
    _userImageView.layer.borderWidth =3.0f;
    _userImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    _userImageView.layer.masksToBounds = YES;
    _userImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    UITapGestureRecognizer* tapGesture= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showEditUserProfile:)];
    [_userImageView addGestureRecognizer:tapGesture];
    _userImageView.userInteractionEnabled = YES;
    
    [IMAGE_MANAGER setImageWithAssetURL:_userImageView assetURL:[DEFAULTS objectForKey:@"userImage"]];
    
    _logView = [[LogInfoView alloc]initWithFrame:CGRectMake(_userImageView.frame.origin.x+_userImageView.frame.size.width+leftMargin, _userName.frame.origin.y, SCREEN_WIDTH-3*leftMargin-_userImageView.frame.size.width, headerHeight-2*10)];
    
    [_headerView addSubview:_logView];
    [_headerView addSubview:_userName];
    
    [self.view addSubview:_headerView];
    [self.view addSubview:_userImageView];
    
    // replace databse test
    
    //    int idindex = 0;
    //    for(int i=0;i<40;i++){
    //        int index = 0;
    //        NSMutableDictionary* dict = CREATE_MUTABLE_DICTIONARY;
    //        [dict setObject:[NSNumber numberWithInt: idindex] forKey:@"id"];
    //        for(NSString* str in UserArray){
    //            [dict setObject:[NSNumber numberWithInt: index] forKey:str];
    //            index++;
    //        }
    //        idindex += 1;
    //        [DATABASE replaceInfoLog:dict];
    //    }
    //
    //
    //    [DATABASE fetchLogInfoWithCompletion:^(BOOL success, NSArray *logObjects) {
    //        DLog(@"%d",[logObjects count]);
    //    }];
    
    //    float f = 0.5564;
    //    NSString* str = [NSString stringWithFormat:@"%.1lf", f];
    //    DLog(@"%@",str);
    
}


-(void) showEditUserProfile:(id)sender
{
    
    
//    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//    {
//        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        imagePicker.delegate = self;
//        [self presentViewController:imagePicker animated:YES completion:nil];
//    }
    
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackgroundImage:nil  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [[UIBarButtonItem appearance] setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    
    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.delegate = (id) self;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker animated:YES completion:^{
        
    }];
    
//    EditProfileViewController* epVC = [EditProfileViewController new];
//    epVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:epVC animated:YES];
//    //    [self.navigationController presentViewController:epVC animated:YES completion:^{}];
}

#pragma mark UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    _userImageView.image = selectedImage;
    
//    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//    
    NSURL *imageurl  = [info objectForKey: UIImagePickerControllerReferenceURL];
    [_library addAssetURL:imageurl toAlbum:@"DiveLog0" withCompletionBlock:^(NSError *error) {
        if(!error){
            [DEFAULTS setObject:[imageurl absoluteString] forKey:@"userImage"];
            [DEFAULTS synchronize];
        }
    }];
    
//    [DEFAULTS setObject:[imageurl absoluteString] forKey:@"userImage"];
//    [DEFAULTS synchronize];

    
//    [self saveImage:selectedImage toAlbum:@"DiveLog3"];
    
//    [library writeImageToSavedPhotosAlbum:[selectedImage CGImage] orientation:(ALAssetOrientation)[selectedImage imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error){
//        if (error) {
//        } else {
//            NSLog(@"url %@", assetURL);
//        }  
//    }];
//    
//    [_library saveImage:selectedImage toAlbum:@"DiveLog2" withCompletionBlock:^(NSError *error)
//    {
//        if (error!=nil){
//            NSLog(@"Big error: %@", [error description]);
//        }}];
////
//    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:NO];
}

-(void) didClickButton{
    TKCardModalViewController *modal = [[TKCardModalViewController alloc] init];
    
    UIView* testView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    [testView setBackgroundColor:[UIColor redColor]];
    modal.subView = testView;
    
    [self presentViewController:modal animated:YES completion:nil];
}






#pragma mark loadCustomAlbum

-(void)saveImage:(UIImage*)image toAlbum:(NSString*)albumName{

    
    
    // Create Album
    [_library addAssetsGroupAlbumWithName:albumName
                                  resultBlock:^(ALAssetsGroup *group) {
                                      NSLog(@"added album:%@", albumName);
                                  }
                                 failureBlock:^(NSError *error) {
                                     NSLog(@"error adding album");
                                 }];

    // Add to Album
    __block ALAssetsGroup* groupToAddTo;
    
    [_library enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                    if ([[group valueForProperty:ALAssetsGroupPropertyName] isEqualToString:albumName]) {
                                        NSLog(@"found album %@", albumName);
                                        groupToAddTo = group;
                                    }
                                }
                              failureBlock:^(NSError* error) {
                                  NSLog(@"failed to enumerate albums:\nError: %@", [error localizedDescription]);
                              }];
    CGImageRef img = [image CGImage];
    [_library writeImageToSavedPhotosAlbum:img
                                      metadata:nil
                               completionBlock:^(NSURL* assetURL, NSError* error) {
                                   if (error.code == 0) {
                                       NSLog(@"saved image completed:\nurl: %@", assetURL);
                                       
                                       // try to get the asset
                                       [_library assetForURL:assetURL
                                                     resultBlock:^(ALAsset *asset) {
                                                         // assign the photo to the album
                                                         [groupToAddTo addAsset:asset];
                                                         NSLog(@"Added %@ to %@", [[asset defaultRepresentation] filename], albumName);
                                                     }
                                                    failureBlock:^(NSError* error) {
                                                        NSLog(@"failed to retrieve image asset:\nError: %@ ", [error localizedDescription]);
                                                    }];
                                   }
                                   else {
                                       NSLog(@"saved image failed.\nerror code %i\n%@", error.code, [error localizedDescription]);
                                   }
                               }];
}

@end

