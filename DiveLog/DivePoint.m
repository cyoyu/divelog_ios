//
//  DivePoint.m
//  DiveLog
//
//  Created by POPO on 1/22/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "DivePoint.h"

@implementation DivePoint

@synthesize name;
@synthesize location;
@synthesize longitude;
@synthesize latitude;
@synthesize type;
@synthesize info;
@synthesize pictures;
@synthesize creatures;
@synthesize times;
@synthesize lastDiveTime;

-(id)init
{
    self = [super init];
    
    name = @"";
    location = @"";
    latitude = 0.0f;
    longitude = 0.0f;
    type = @"";
    info = @"";
    pictures = @"[]";
    creatures = @"[]";
    times = 0;
    lastDiveTime = 0;
    
    return self;
}


+(DivePoint*)getDivePointWithDict:(NSDictionary*)dict
{
    DivePoint* point = [DivePoint new];
    NSLog(@"%@",dict);
    
    point.longitude = [dict[@"longitude"] floatValue];
    point.latitude = [dict[@"latitude"] floatValue];
    point.name = dict[@"name"];
    point.location = dict[@"location"];
    
    return point;
}

-(NSDictionary*) getDictFromDivePoint
{
    NSDictionary* dict = @{@"name":name,
                           @"location":location,
                           @"latitude":[NSNumber numberWithFloat:latitude],
                           @"longitude":[NSNumber numberWithFloat:longitude],
                           };
    
    NSLog(@"%@",dict);
    return dict;
}


@end
