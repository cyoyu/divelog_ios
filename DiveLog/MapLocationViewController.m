//
//  MapViewController.m
//  DiveLog
//
//  Created by York on 1/21/15.
//  Copyright (c) 2015 yokusama. All rights reserved.
//

#import "MapLocationViewController.h"
#import "Constant.h"
#import <MapKit/MapKit.h>
#import "BlocksKit+UIKit.h"
#import "MKPointAnnotation+Helper.h"
#import "MyAnnotation.h"

@implementation MapLocationViewController

@synthesize locationSelectionCallback;

- (id)init
{
    self = [super init];
    if (self != nil){
        _divePoints = CREATE_MUTABLE_ARRAY;
        _firstUpdate = YES;
        
    }
    return self;
}


-(void)viewDidLoad
{
    
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addLocation)];
    
    
    
    _mapView = [[MKMapView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];

    
    _pinImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"pink_pin"]];
    _pinImageView.frame = CGRectMake(SCREEN_WIDTH/2-8, SCREEN_HEIGHT/2-NAVI_BAR_HEIGHT, 16, 24);
    _pinImageView.center = _mapView.center;
    _pinImageView.frame = CGRectMake(_pinImageView.frame.origin.x, _pinImageView.frame.origin.y-12,16, 24);

    MKCoordinateRegion region;
    MKCoordinateSpan mapSpan;
    mapSpan.latitudeDelta =0.025;
    mapSpan.longitudeDelta =0.025;
    region.span = mapSpan;
    if([GET_DEFAULT(ADD_DIVE_LATITUDE) floatValue]!=0 &&[GET_DEFAULT(ADD_DIVE_LONGITUDE) floatValue]!=0){
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([GET_DEFAULT(ADD_DIVE_LATITUDE) floatValue], [GET_DEFAULT(ADD_DIVE_LONGITUDE) floatValue]);
        region.center = coordinate;
        _firstUpdate = NO;
        _mapView.region = region;
        _pinImageView.hidden = YES;
    }
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:0.1f]];
    
    [self.view addSubview:_mapView];
    [self.view addSubview:_pinImageView];
    
    [self reloadAnnotation];
}



-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _pinImageView.frame = CGRectMake(SCREEN_WIDTH/2-9, SCREEN_HEIGHT/2-NAVI_BAR_HEIGHT-STATUS_BAR_HEIGHT-28, 18, 28);
 
 //   [_location stopUpdatingLocation];
}


- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    if(_pinImageView.isHidden){
        _pinImageView.hidden = NO;
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)reloadAnnotation
{
    int index = 0;
    for(DivePoint* points in _divePoints){
        
        MyAnnotation *annotation = [[MyAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake(points.latitude, points.longitude);
        annotation.title = points.name;
        annotation.subtitle = points.location;
        annotation.tag = index;
        [_mapView addAnnotation:annotation];
        index += 1;
        if(points.latitude == [GET_DEFAULT(ADD_DIVE_LATITUDE) floatValue] && points.longitude == [GET_DEFAULT(ADD_DIVE_LONGITUDE) floatValue]){
            [_mapView selectAnnotation:annotation animated:FALSE];
        }
    }
}

-(void)getLocationFromCoordinate:(float)latitude lagitude:(float)longitude completion:(void(^)(NSString* country, NSString* city))callback
{
    
    CLLocation *newLocation=[[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
    
    CLGeocoder *geocoder=[[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray* placemarks, NSError* error)
     {
         CLPlacemark*placemark = [placemarks objectAtIndex:0];
         
         NSString *address = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         NSString *countryName = placemark.country;
         NSString *cityName = placemark.locality;
         NSString *ISOcountryCode = placemark.ISOcountryCode;
         NSString *postalCode = placemark.postalCode;
         NSString *administrativeArea = placemark.administrativeArea;
         NSString *subLocality = placemark.subLocality;
         NSString *subThoroughfare = placemark.subThoroughfare;
         
         callback(countryName,cityName);
         
         NSLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
         NSLog(@"placemark.country %@",placemark.country);
         NSLog(@"placemark.postalCode %@",placemark.postalCode);
         NSLog(@"placemark.administrativeArea %@",placemark.administrativeArea);
         NSLog(@"placemark.locality %@",placemark.locality);
         NSLog(@"placemark.subLocality %@",placemark.subLocality);
         NSLog(@"placemark.subThoroughfare %@",placemark.subThoroughfare);
         
     }];
}


-(void) addLocation{
    
    double X = _mapView.centerCoordinate.latitude;
    double Y = _mapView.centerCoordinate.longitude;
    
    [self getLocationFromCoordinate:X lagitude:Y completion:^(NSString *country, NSString *city) {
        
        
        // CardViewController 排版
        TKCardModalViewController *modal = [[TKCardModalViewController alloc] init];
        
        UIView* bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 280, 200)];
        [bgView setBackgroundColor:[UIColor clearColor]];
        
        UILabel* titleLabel = [UILabel new];
        UILabel* locationLabel = [UILabel new];
        UILabel* nameLabel = [UILabel new];

        UITextField* divepointName = [UITextField new];
        UITextField* divepointLocation = [UITextField new];
        UIButton* doneButton = [UIButton new];
        [titleLabel setText:@"潛點資訊"];
        [locationLabel setText:@"地點:"];
        [nameLabel setText:@"名稱:"];
        if(country!=NULL){
            if(city!=NULL){
                [divepointLocation setText:[NSString stringWithFormat:@"%@%@",country,city]];
            }else{
                [divepointLocation setText:country];
            }
        }
        
        [divepointLocation setPlaceholder:@"請輸入地點"];
        divepointLocation.layer.borderWidth = 0.5f;
        divepointLocation.layer.borderColor = [[UIColor grayColor] CGColor];
        divepointLocation.layer.cornerRadius = 10.0f;
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        divepointLocation.leftView = paddingView;
        divepointLocation.leftViewMode = UITextFieldViewModeAlways;
        
        [divepointName setPlaceholder:@"請輸入名稱"];
        divepointName.layer.borderWidth = 0.5f;
        divepointName.layer.borderColor = [[UIColor grayColor] CGColor];
        divepointName.layer.cornerRadius = 10.0f;
        UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
        divepointName.leftView = paddingView2;
        divepointName.leftViewMode = UITextFieldViewModeAlways;
        
        titleLabel.frame = CGRectMake(20, 10, 240, 30);
        titleLabel.textAlignment = NSTextAlignmentLeft;
        locationLabel.frame = CGRectMake(20, 50, 60, 30);
        divepointLocation.frame = CGRectMake(80, 50, 170, 30);
        nameLabel.frame = CGRectMake(20, 90, 60, 30);
        divepointName.frame = CGRectMake(80, 90, 170, 30);

        doneButton.frame = CGRectMake(20, 140, 240, 40);
        
        [doneButton setBackgroundImage:[[UIImage imageNamed:@"btn_blue_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)] forState:UIControlStateHighlighted];
        [doneButton setBackgroundImage:[[UIImage imageNamed:@"btn_blue"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)] forState:UIControlStateNormal];
        [doneButton setTitle:@"確認" forState:UIControlStateNormal];
        [bgView addSubview:titleLabel];
        [bgView addSubview:divepointName];
        [bgView addSubview:divepointLocation];
        [bgView addSubview:doneButton];
        [bgView addSubview:locationLabel];
        [bgView addSubview:nameLabel];
        
        [doneButton addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
            
            if([divepointLocation.text isEqualToString:@""] || [divepointName.text isEqualToString:@""]) return;
            
//             *annotation = [[MKPointAnnotation alloc] init];
//            annotation.coordinate = CLLocationCoordinate2DMake(X, Y);
//            annotation.title = divepointName.text;
//            [_mapView addAnnotation:annotation];
//            
            [modal hide];
            [self.navigationController popViewControllerAnimated:YES];
            
            DivePoint* divepoint = [DivePoint new];
            divepoint.name = divepointName.text;
            divepoint.location = divepointLocation.text;
            divepoint.latitude = X;
            divepoint.longitude = Y;
            divepoint.type = @"";
            [DATABASE replaceIntoDivePointDB:[divepoint getDictFromDivePoint]];
            [DEFAULTS setObject:[NSNumber numberWithFloat:X] forKey:ADD_DIVE_LATITUDE];
            [DEFAULTS setObject:[NSNumber numberWithFloat:Y]  forKey:ADD_DIVE_LONGITUDE];
            locationSelectionCallback(divepoint);
            
        }]];
        
        modal.subView = bgView;
        
        [self presentViewController:modal animated:YES completion:nil];
        
    }];
}




#pragma mark MKMapViewDelegate

// Customize pin view
//-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
//    [self.mapView setCenterCoordinate:userLocation.coordinate animated:YES];
//}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]])
    {
        MKPinAnnotationView *pinView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.animatesDrop = NO;
            pinView.canShowCallout = YES;
            pinView.pinColor = MKPinAnnotationColorPurple;

            // 顯示不同的 view
          //pinView.image = [UIImage imageNamed:@"icon-dive-flag.jpg"];
            UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
            [rightButton setTag:((MyAnnotation*)annotation).tag];

            pinView.rightCalloutAccessoryView = rightButton;
            
            [rightButton addTarget:self action:@selector(didSelectDivePoint:) forControlEvents:UIControlEventTouchUpInside];
            
            
            //DLog(@"%d:%@",((MyAnnotation*)annotation).tag,((DivePoint*)_divePoints[((MyAnnotation*)annotation).tag]).name);
            
            // view 加上 offset
//            pinView.calloutOffset = CGPointMake(0, 32);
            
            // 左邊加上icon
//            UIImageView *iconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pizza_slice_32.png"]];
//            pinView.leftCalloutAccessoryView = iconView;
            
            
        } else {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    return nil;
}


- (void) mapView:(MKMapView*)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    // 將中心設成所在地
//    NSLog(@"update lat: %f long: %f",userLocation.location.coordinate.latitude,userLocation.location.coordinate.longitude);
    MKCoordinateRegion region;
    region.center = userLocation.location.coordinate;
    MKCoordinateSpan mapSpan;
    mapSpan.latitudeDelta =0.025;
    mapSpan.longitudeDelta =0.025;
    region.span = mapSpan;
    
    if(_firstUpdate){
        _mapView.region = region;
        _firstUpdate = NO;
    }
}

#pragma mark didSelectPoint
-(void)didSelectDivePoint:(UIButton*)sender
{
    DivePoint* divePoint = _divePoints[sender.tag];
    [DEFAULTS setObject:[NSNumber numberWithFloat:divePoint.latitude] forKey:ADD_DIVE_LATITUDE];
    [DEFAULTS setObject:[NSNumber numberWithFloat:divePoint.longitude]  forKey:ADD_DIVE_LONGITUDE];
    locationSelectionCallback(divePoint);
    [self.navigationController popViewControllerAnimated:YES];
}

//#pragma mark CLLocationManagerDelegate
//
//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    MKCoordinateRegion region = { { 0.0, 0.0 }, { 0.0, 0.0 } };
//    region.center.latitude = manager.location.coordinate.latitude;
//    region.center.longitude = manager.location.coordinate.longitude;
//    region.span.latitudeDelta = 0.0187f;
//    region.span.longitudeDelta = 0.0137f;
//    //[_mapView setRegion:region animated:YES];
//}
//
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation    *)newLocation fromLocation:(CLLocation *)oldLocation{
//    //some other stuff here
//    [_mapView setShowsUserLocation:YES];
//}


#pragma mark TextViewDelegate
#pragma mark UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    [SINGLETON dialogAnimationUp:75];
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [SINGLETON dialogAnimationUp:-75];
    
}

@end
