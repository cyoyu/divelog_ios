//
//  Singleton.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "Singleton.h"
#import "LogTableViewController.h"
#import "SettingViewController.h"
#import "MainViewController.h"
#import "MapViewController.h"
#import "YCNavigationController.h"

@implementation Singleton

@synthesize dialogBgView;


-(void) customizeInterface
{
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor,
                                               [UIColor clearColor], UITextAttributeTextShadowColor,
                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
    
    [[UINavigationBar appearance] setBarTintColor:NAVY];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    [[UINavigationBar appearance] setTintColor:[UIColor clearColor]];
    
    [[UITabBar appearance] setBarTintColor:LIGHT_GRAY_COLOR];
    [[UITabBar appearance] setTintColor:LIGHT_GRAY_COLOR];
    
    [[UIBarButtonItem appearance] setTintColor:WHITE_COLOR];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
}


+(id)sharedInstance
{
    static Singleton* sharedSingleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSingleton = [[self alloc]init];
    });
    return sharedSingleton;
}

-(id)init
{
    if(self = [super init])
    {
        // init here
        _mainTabBarViewController = [UITabBarController new];
        _dbManager = [DatabaseManager new];
        _imgManager = [ImageManager new];
        _defaults = [NSUserDefaults standardUserDefaults];
        
        
    }
    return self;
}

-(void) setup
{
    
    NSDictionary * textAttributes =    [NSDictionary dictionaryWithObjectsAndKeys:
                                        GRAY_COLOR, UITextAttributeTextColor,
                                        [UIColor clearColor], UITextAttributeTextShadowColor,
                                        [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                        SYSTEM_FONT_WITH_SIZE(10.0), UITextAttributeFont,nil];
    
    NSDictionary * highLightTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                              MAIN_COLOR, UITextAttributeTextColor,
                                              [UIColor clearColor], UITextAttributeTextShadowColor,
                                              [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
                                              SYSTEM_FONT_WITH_SIZE(10.0), UITextAttributeFont,nil];
    
    
    MainViewController* mainVC = [MainViewController new];
    UINavigationController * mainNav = [[UINavigationController alloc]initWithRootViewController:mainVC];
    UITabBarItem *tab1 = [[UITabBarItem alloc] initWithTitle:@"Home" image:[[UIImage imageNamed:@"nav_icon5_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"nav_icon5_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab1 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab1 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [mainNav setTabBarItem:tab1];
    [tab1 setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    
    
    LogTableViewController* logVC = [LogTableViewController new];
    YCNavigationController * logNav = [[YCNavigationController alloc]initWithRootViewController:logVC];
    
    
    UITabBarItem *tab2 = [[UITabBarItem alloc] initWithTitle:@"DiveLog" image:[[UIImage imageNamed:@"nav_icon4_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"nav_icon4_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab2 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab2 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [logNav setTabBarItem:tab2];
    
    
    MapViewController* mapVC = [MapViewController new];
    UINavigationController * mapNav = [[UINavigationController alloc]initWithRootViewController:mapVC];
    UITabBarItem *tab3 = [[UITabBarItem alloc] initWithTitle:@"Map" image:[[UIImage imageNamed:@"nav_icon1_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"nav_icon1_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab3 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab3 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [mapNav setTabBarItem:tab3];
    
    
    SettingViewController* setVC = [SettingViewController new];
    YCNavigationController * setNav = [[YCNavigationController alloc]initWithRootViewController:setVC];
    UITabBarItem *tab4 = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[[UIImage imageNamed:@"nav_icon5_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"nav_icon5_noraml"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tab4 setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    [tab4 setTitleTextAttributes:highLightTextAttributes forState:UIControlStateSelected];
    [setNav setTabBarItem:tab4];
    
//    _mainTabBarViewController.tabBar.translucent = NO;
    _mainTabBarViewController.viewControllers = @[mainNav, logNav,mapNav,setNav];
    
}


-(NSString*)getCurrentMonth
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM"];
    NSString *monthString = [dateFormat stringFromDate:today];
    return monthString;
}

-(NSString*)getCurrentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd"];
    NSString *dateString = [dateFormat stringFromDate:today];
    return dateString;
}

-(NSString*)getCurrentFullDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:today];
    return dateString;
}

-(NSString*)getCurrentYear
{
    NSDate *year = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    NSString *yearString = [dateFormat stringFromDate:year];
    return yearString;
}

-(NSString*)getCurrentTime
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH-mm"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    return resultString;
}

-(NSString*)getCurrentHour
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    return resultString;
}

-(NSString*)getCurrentMinute
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"mm"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    return resultString;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


-(void) removeDialogImmediately
{
    if(dialogBgView!=nil) {
        [dialogBgView removeFromSuperview];
        dialogBgView = nil;
    }
}


-(UIView*) getStatusBarWindowView
{
    UIWindow *statusBarWindow = (UIWindow *)[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    
    return statusBarWindow;
}

-(void) showTextDialog:(NSString*)title withSubtitle:(NSString*)subtitle  withDelegate:(id<UITextViewDelegate>)delegate withCompletion: (void (^)(NSString* message)) callback

{
    [self removeDialogImmediately];
    
    UIView* topView = [self getStatusBarWindowView];
    dialogBgView = [[UIView alloc] initWithFrame:topView.frame];
    dialogBgView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
    dialogBgView.userInteractionEnabled = YES;
    
    [topView addSubview:dialogBgView];
    
    dialogBgView.alpha = 0.0;
    dialogBgView.userInteractionEnabled = YES;
    dialogBgView.clipsToBounds = NO;
    
    UIImageView* dialogBoxView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"dialog_white"] resizableImageWithCapInsets:UIEdgeInsetsMake(12, 12, 12, 12)]];
    
    dialogBoxView.frame = CGRectMake((topView.frame.size.width-280)/2, 0, 280, 200);
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    dialogBoxView.frame = CGRectMake((topView.frame.size.width-280)/2, 0, 280, 200);
    dialogBoxView.userInteractionEnabled = YES;
    [dialogBgView addSubview:dialogBoxView];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 7.5, 280, 50)];
    titleLabel.text = title;
    titleLabel.textColor = PINK_COLOR;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = BOLD_FONT_WITH_SIZE(24.0);
    titleLabel.numberOfLines = 0;
    titleLabel.userInteractionEnabled = NO;
    [dialogBoxView addSubview:titleLabel];
    
    UILabel* subtitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, 240, 50)];
    subtitleLabel.text = subtitle;
    subtitleLabel.textColor = GRAY_COLOR;
    subtitleLabel.backgroundColor = [UIColor clearColor];
    subtitleLabel.textAlignment = NSTextAlignmentCenter;
    subtitleLabel.font = SYSTEM_FONT_WITH_SIZE(10.0);
    subtitleLabel.numberOfLines = 0;
    subtitleLabel.userInteractionEnabled = NO;
    [dialogBoxView addSubview:subtitleLabel];
    
    UITextView* textView = [[UITextView alloc]initWithFrame:CGRectMake(20, 105, 240, 100)];
    [dialogBoxView addSubview:textView];
    textView.layer.borderColor = [BLACK_COLOR CGColor];
    textView.layer.cornerRadius = 10.0f;
    textView.layer.borderWidth = 0.5;
    textView.delegate = delegate;
    
    [dialogBoxView addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [textView resignFirstResponder];
    }]];
    
    UIButton* doneBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 215, 240, 40)];
    [doneBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_normal"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateNormal];
    [doneBtn setBackgroundImage:[[UIImage imageNamed:@"btn_blue_down"] resizableImageWithCapInsets:UIEdgeInsetsMake(32, 32, 32, 32)] forState:UIControlStateHighlighted];
    [doneBtn setTitle:@"確認" forState:UIControlStateNormal];
    [dialogBoxView addSubview:doneBtn];
    
    [doneBtn addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        if(textView.text.length!=0){
            callback(textView.text);
            [UIView animateWithDuration:0.15 animations:^{
                dialogBgView.alpha = 0.0;
                dialogBoxView.transform = CGAffineTransformMakeScale(0, 0);
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [dialogBgView removeFromSuperview];
                dialogBgView = nil;
            });
            
        }
    }]];
    
    dialogBoxView.frame = CGRectMake((topView.frame.size.width-280)/2, (topView.frame.size.height-320)/2-20, 280, 265);
    
    UIImageView* cancelButton = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"close"]];
    cancelButton.userInteractionEnabled = YES;
    cancelButton.frame = CGRectMake(dialogBoxView.frame.origin.x+dialogBoxView.frame.size.width-26, dialogBoxView.frame.origin.y-18, 50, 50);
    cancelButton.contentMode = UIViewContentModeCenter;
    [cancelButton addGestureRecognizer:[[UITapGestureRecognizer alloc] bk_initWithHandler:^(UIGestureRecognizer *sender, UIGestureRecognizerState state, CGPoint location) {
        [UIView animateWithDuration:0.15 animations:^{
            dialogBgView.alpha = 0.0;
            dialogBoxView.transform = CGAffineTransformMakeScale(0, 0);
        }];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dialogBgView removeFromSuperview];
            dialogBgView = nil;
        });
    }]];
    
    [dialogBgView addSubview:cancelButton];
    
    cancelButton.center = dialogBoxView.center;
    cancelButton.transform = CGAffineTransformMakeScale(0, 0);
    dialogBoxView.transform = CGAffineTransformMakeScale(0, 0);
    
    [UIView animateWithDuration:0.15 animations:^{
        dialogBgView.alpha = 1.0;
        dialogBoxView.transform = CGAffineTransformMakeScale(1.0, 1.0);
        cancelButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
        cancelButton.frame = CGRectMake(dialogBoxView.frame.origin.x+dialogBoxView.frame.size.width-26, dialogBoxView.frame.origin.y-18, 50, 50);
        
        
    }];
}

-(void) dialogAnimationUp:(int)range
{
    [UIView animateWithDuration:0.2 animations:^{
        dialogBgView.frame = CGRectMake(dialogBgView.frame.origin.x, dialogBgView.frame.origin.y
                                        -range, dialogBgView.frame.size.width, dialogBgView.frame.size.height);
    }];
}


@end
