//
//  EditProfileViewController.m
//  DiveLog
//
//  Created by York on 12/11/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "EditProfileViewController.h"
#import "Constant.h"
@interface EditProfileViewController ()

@end

@implementation EditProfileViewController





- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self addCustomNavigationBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
