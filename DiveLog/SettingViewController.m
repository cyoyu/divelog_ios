//
//  SettingViewController.m
//  DiveLog
//
//  Created by York on 12/8/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "SettingViewController.h"
#import "Constant.h"
#import "NotificationViewController.h"

@implementation SettingViewController

- (id)init
{
    self = [super init];
    if (self != nil){
    }
    return self;
}


-(void)viewDidLoad
{
    [super viewDidLoad];
    [self configureViewForIOS7];
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:142.0/255.0 blue:174.0/255.0 alpha:0.1f]];
    
    _testBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 10, 100, 30)];
    [_testBtn setTitle:@"testing" forState:UIControlStateNormal];
    [_testBtn setBackgroundColor:MAIN_COLOR];

    [_testBtn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_testBtn];
    

    
    //    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    //    testObject[@"foo"] = @"yoyo";
    //    testObject[@"cool"] = @"yes";
    //    testObject[@"test3"] = @"Sandy";
    //
    //    [testObject saveInBackground];
}

-(void) doneAction:(id)sender
{
    NotificationViewController* notifVC =  [NotificationViewController new];
    [self.navigationController pushViewController:notifVC animated:YES];
}

@end
