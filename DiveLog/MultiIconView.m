//
//  IconButtonView.m
//  DiveLog
//
//  Created by York on 12/20/14.
//  Copyright (c) 2014 yokusama. All rights reserved.
//

#import "MultiIconView.h"
#import "UIImage+Helper.h"

@implementation MultiIconView

@synthesize delegate;
@synthesize viewMode;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    _btnMode = 0;
    _btnsArray = CREATE_MUTABLE_ARRAY;
    viewMode = MULTI_SELECT;
    return self;
}

-(void) reloadUIwithImages:(NSArray*)imgArrays btnTitle:(NSArray*)titles selected:(NSSet*)set
{
    if([imgArrays count] != [titles count]) return;
    
    int nextX=5;
    int lines = ((int)[imgArrays count]/4)+1;
    int lineIndex = 0;
    int widthCount = [imgArrays count]>3? 3:(int)[imgArrays count];
    
    for(int i=0;i<[imgArrays count];i++){
        
        UIButton* btn = [[UIButton alloc]initWithFrame:CGRectMake(nextX,lineIndex*30+5, 75, 25)];
        [btn setImage:[[UIImage imageNamed:imgArrays[i]] toGrayscale] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:imgArrays[i]] forState:UIControlStateSelected];

        [btn setTitle:[titles objectAtIndex:i] forState:UIControlStateNormal];
        btn.titleLabel.textAlignment = NSTextAlignmentLeft ;
        [btn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateSelected];
        [btn.titleLabel setFont:SYSTEM_FONT_WITH_SIZE(14)];
        btn.layer.borderColor = [[UIColor darkGrayColor]CGColor];
        btn.layer.borderWidth = 0.5f;
        btn.layer.cornerRadius = 10.0f;
        [btn setTag:i];
        
        if([set containsObject:imgArrays[i]]){
            [btn setSelected:YES];
            btn.alpha = 1.0;
        }else{
            btn.alpha = 0.5;
        }
        
        [btn addTarget:self action:@selector(didClickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [_btnsArray addObject:btn];
        [self addSubview:btn];
        nextX += 80;
        
        if((i+1)%3==0)
        {
            lineIndex += 1;
            nextX = 5;
        }
    }
    self.frame = CGRectMake(SCREEN_WIDTH-widthCount*80-20,self.frame.origin.y, widthCount*80, 30*lines+1);
    
}


-(void) didClickBtn:(id)sender
{
    UIButton* clickedBtn = sender;
    _btnMode = (int)clickedBtn.tag;
    
    // MultiSelection
    if(viewMode==MULTI_SELECT){
    if(clickedBtn.isSelected){
        [clickedBtn setSelected:NO];
        clickedBtn.alpha = 0.5;
    }else{
        [clickedBtn setSelected:YES];
        clickedBtn.alpha = 1.0;
    }
    }else if(viewMode ==SINGLE_SELECT){
        
        for(int i=0;i<[_btnsArray count];i++){
            UIButton* btn = [_btnsArray objectAtIndex:i];
            if(i!=_btnMode){
                [btn setSelected:NO];
                btn.alpha = 0.5;
            }else{
                if(btn.isSelected){
                    [btn setSelected:NO];
                    btn.alpha = 0.5;
                }
                else{
                    [btn setSelected:YES];
                    btn.alpha = 1.0;
                }
            }
        }
    }
    
    [delegate didClickMultiIcon:_btnMode onView:self];
}

@end
